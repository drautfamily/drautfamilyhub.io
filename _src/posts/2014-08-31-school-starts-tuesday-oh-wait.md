    Title: School Starts Tuesday! Oh wait...
    Date: 2014-08-31T14:55:31
    Tags: Weekly Letters

Hey Family,

Hope you're all doing well! We've had a pretty good week here, it feels like it has been too busy again though. :) Sadly the week ended with Brittney and Abby coming down with colds, but they're on the upswing now, so we're grateful for that.

Work has been good this week. I attended a training for a tool called AppDynamics on Monday and Tuesday which was very informative. AppDynamics is what I'm doing my presentation on at SORT in October, so I was grateful for the opportunity to get some formal training on it. The rest of the week I went back to the normal routine. My on-call rotation ended on Tuesday, so I'm back to working on our main projects. I feel much more confident and aware of everything now too, which is very nice. I'm not sure if it was due to the on-call shift, or if 4 months has finally been enough time for my mind to get fully saturated. :) Either way, work is much smoother, and I'm enjoying it. :)

Tuesday night Brittney picked me up from work in SLC and we headed up to Roy for the family pool party. That was a lot of fun. Not a huge turnout, but all the guys were having a really fun time trying to see who could skim the water the furthest coming down from the water slide. A couple of our cousins could make it all the way to the rope separating the slide area from the regular pool area. It was nuts!

Wednesday was Heather's birthday, so we had cake and ice cream and presents for her. She had a class till 8:30 though, so it was a short celebration. By 10 we were all zombies. :) Thursday Michael invited a ton of his MTC friends over to play lazer tag. We actually used all 12 guns! That was the first time Liberty Square felt too small! It was crazy! It was pretty dark for the last couple of games, so everyone hung out in the parking garage where there was plenty of light. There was so much action!

Friday night our friends Chris and Mercedes came over to play board games. It was fun to catch up with them. Saturday was a busy chore day. We cleaned the refrigerator, cleaned up Abby's room, went grocery shopping, etc. We also watched Miracle and hung out with Kristine. While we were grocery shopping at Macey's, we ended up getting a bunch of food storage as well. They're starting their annual case lot sale. We guessed on a few things, we couldn't remember exactly how much we had. Now we're well overstocked on Cream of Mushroom. :) It inspired me to write a simple webpage that we can use to track our food storage. That's probably what I'll be doing tomorrow. :) Brittney hopes I actually do it, she'd love it. :)

Church today was good. I taught an impromptu Gospel Doctrine lesson, it actually went pretty well. The rest was great too. Now we're just resting, hoping Brittney and Abby are totally better by tomorrow. :) Love you all!
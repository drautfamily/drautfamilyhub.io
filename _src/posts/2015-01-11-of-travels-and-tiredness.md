    Title: Of Travels and Tiredness
    Date: 2015-01-11T10:04:18
    Tags: Weekly Letters

Hey Family,

Hope you're all doing well! It's been a good week for us. We're enjoying having Mom out here. :)

<!-- more -->

Monday night for FHE we talked about goals, and we both set some goals we wanted to work on. It's so nice to have something you're working towards! One of my goals was to stop sleeping in and start getting up early again, so that I would have time to read the scriptures before rushing out the door to catch the bus. It's been good to get back on schedule, but at the same time, I'm so tired! I figure another week or two and my body will be used to it again. :)

Brittney and Abby have been doing well too. Abby loves Cheerios! Brittney loves that Cheerios keep Abby quiet! :) Abby has been getting back on a nap schedule, so that's nice for all of us. Brittney is keeping busy taking care of her, doing activity days, and processing all the paperwork that shows up in our mailbox. (Financial planning is no fun!) 

Mom showed up on our couch one morning this week..I don't recall which day. :) I left for work before she woke up though. Brittney said she was happy to see Abby before heading up to Logan.

Friday night Brittney and I went to the Temple together while Heather watched Abby for us. It was good to go together. When we came back, Kristine was there too. They stayed and hung out for a little while. Kristine kept trying to convince Heather to go dancing with her, it was pretty amusing. :) It was about midnight before we got to bed. 7:30 comes around the next morning and I hopped out of bed. We've got to go! Mike was expecting us to pick him up at 8:30, so we hurried and got the three of us ready, and headed out. We got up to Logan for Grandpa's funeral around 10:40. It was good to see so much family again. We enjoyed catching up with Aunts and Uncles and cousins. Michael's vocal solo was wonderful, as was Mom's talk. I enjoyed hearing everyone's stories about Grandpa. It made me wish I had gotten to know him a little better. After the services and luncheon, we left around 3:30 and got back home around 5:30.

I think we were all exhausted, but everyone came over anyway to be together for a bit. We ate dinner and talked and played some Mario Kart with everyone. It was good. :) Today should be pretty normal. Church at 1, dinner at 5, and then some additional meetings today. Love you all!



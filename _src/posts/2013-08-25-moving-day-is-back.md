    Title: Moving Day is back!
    Date: 2013-08-25T14:57:55
    Tags: Weekly Letters

Hey Family,

Hope you're all doing well, we've had a good week here. Brittney's students came back to school on Monday, so this has been her first full week back. She likes her class, but is a little tired from spending full days on her feet again. She's looking forward to having a classroom routine again. :)

Work has been fun this week, I've just been working on tidying up the projects I did this summer and getting them ready for production use. My last day is going to be Wednesday. I've learned a ton this summer, it has been good. I've been working on Greebles more this week, I'm planning on putting an update out on <a href="http://drautb.wordpress.com" target="_blank">my other blog</a>, so check that too a little later. :)

I picked Mindy up from the airport on my way home from work on Tuesday. She stayed with us that night and then we dropped her off at the MTC on Wednesday. She seemed tired of waiting. :) We went out to lunch at Panda Express with Grandma and Michael before dropping her off. It was fun to see Grandma again. I put all the pictures up on Google+ already, so you should have been able to see those.

Friday night we went to the MTC as usual, and then came home and watched a movie with Kristine. It was nice just to relax. Saturday morning we got up and helped Kristine move all her stuff into her new apartment, which only took an hour or so. After that Brittney and I went to the Temple. It was our first time doing a session in a month or so, we really enjoyed it. We're looking forward to going back this week. That afternoon I worked on Greebles while Brittney took a nap, and then we went shopping and ran a couple of errands. It was a pretty calm day.

Church today was good. The theme for the past month or so it seems has been on strengthening marriage and families. It has been really good. This afternoon a couple of Michael's friends, and Robert and Kristine are coming over for dinner, so it'll be a lively evening. :) We love you!
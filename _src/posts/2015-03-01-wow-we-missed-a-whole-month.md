    Title: Wow, We Missed A Whole Month!
    Date: 2015-03-01T11:54:00
    Tags: Weekly Letters

This is just for you, Becky! :)

<!-- more -->

Abby has been growing up a lot. She's started taking first steps on her own, without any coaxing from us. She'll take one or two small steps and then start crawling again. :) She's also been watching [Baby Signing Time][1] a lot, which she loves. We're hoping she'll start using the signs for things that she wants, rather than just throwing a fit. That's something that has started in the last few weeks too, major temper tantrums. :) Anyways, she enjoys watching the show and seeing the other babies sign things. She'll make the signs for eating and dog. :) Recently she's started trying to say the words that they say, rather than the signs. (Like cracker) She had another checkup a few weeks ago too. She's still really small for her age, but everything else is good.

Brittney has had a busy couple of weeks. She was released last week from being the Activity Days leader, and was called to be 1st counselor in the Young Women's presidency. She was excited, but a little nervous about starting something new. She's teaching the YW today for the first time. She's had a really busy week with the transition.

Work has been going well for me. Our team is working better together and we're getting more done, so everyone is enjoying that. It's strange to think that I'm coming up on a year there.

A few weeks ago we went down to Grandma's with Michael, Mike, and Mindy to visit Kim and her children. It was a lot of fun to meet that side of the family. They were all extremely nice and fun to talk with. Grandma said they felt the same way and want to get together again soon, so maybe we'll have a game night or something sometime.

Two weeks ago we babysat the Moore's kids for a week while their parents went to Hawaii. Brittney usually tutors their son, Jordan, every week. (She had him in her class one year.) That was interesting. :) Jordan is the youngest at 11, Tyler is 15, and Megan is 18. It was fun being in a house of teenagers playing the parents instead of the siblings... :D We had fun though, they enjoyed playing board games with us or watching movies. Overall it a was a good week. One evening we went to Airborne with Jordan. (It's one of those indoor trampoline arenas) They had an American Ninja Warrior obstacle course that we tried a few times, it was tough!

It's been getting cold here again recently, we don't know what to expect from the weather anymore. We're looking forward to the Summer when you guys move out here. We're also planning a trip out to see Brittney's family. Lots of good stuff coming up.

Yesterday we went to Michael's sophmore recital. He did really well. The singing was good, but on top of that, he and his partner made it a really entertaining performance. (The other recitals I've been to have been pretty dry.) I'll see if I can share the videos I took later today. Love you all!

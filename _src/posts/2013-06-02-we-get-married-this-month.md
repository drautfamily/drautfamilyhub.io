    Title: We get married this month!
    Date: 2013-06-02T16:09:22
    Tags: Weekly Letters, Pictures

Hey Family!

Happy Birthday Becky and Michael! We're glad you liked the watch Becky, and Michael, we'll get you something when you're back. :) Maybe free Sunday dinners next school year or something. :)

Guess what! We get married in 12 days! This week has felt super busy, but it's probably been just like the others. :) We had an awesome Memorial day holiday: We found a mattress that we liked on sale, went for a bike ride, went swimming, and found a couch that we liked on KSL. (In good condition) It was a good day, though Brittney's house feels really crowded now with an extra couch and a queen size mattress and box leaning against the wall. :)

The rest of the week was mostly routine, lots of work during the day and errands in the evening. Tuesday night they delivered our mattress, and Brittney went visiting teaching. Wednesday night we went to see my Bishop for my living ordinance recommend, which was good. He gave us some good counsel. After that we went to a field with Kristine and just played catch with her lacrosse stuff.

Thursday evening Brittney's school had a film festival where all the teachers were showing the films that their classes made earlier in the year. I just helped her with some preparations for this week in between visiting with the parents that came. We never want to see her class's film again though. :)

Friday was awesome. Brittney's ward had a bonfire activity in this meadow way up in the mountains behind Squaw Peak. It was gorgeous. Here's the best part: There is an amazing downhill mountain biking trail that starts right where our activity was, so five of us brought our bikes and rode down it twice. It was intense! Me and Joseph, the only two who hadn't ridden the trail before, were slower than the others, but we only crashed once! Both in the same spot. :) The second run was much better. Here's a <a href="http://www.youtube.com/watch?v=Cx-QBiYdbos" target="_blank">youtube video</a> that someone else made of the trail.

Saturday was a pretty busy day too. Brittney spent the morning shopping with Kristine while I worked on lot's of little things I needed to do. We also went to T-Mobile and set up our phone plan, so that's all taken care of. Luckily we were able to keep our phone numbers, but mine is still in transition from the prepaid plan to our current plan, so that's why I have the temp number I sent out yesterday. :) We also doubled with my friend Dave and his unofficial fiance, Jenny. We made dinner and then went canoeing on the river, it was enjoyable, and there weren't any pesky bugs. :)
<p style="text-align:center;"><a href="https://lh3.googleusercontent.com/-S1NUyJEKMY0/Uaq0s54iPFI/AAAAAAAABMk/GJiK0daPjkA/w671-h503-no/IMG_20130601_200457.jpg"><img class="aligncenter" alt="" src="https://lh3.googleusercontent.com/-S1NUyJEKMY0/Uaq0s54iPFI/AAAAAAAABMk/GJiK0daPjkA/w671-h503-no/IMG_20130601_200457.jpg" width="145" height="109" /></a></p>
After the date we watched a couple episodes of White Collar while Brittney transferred numbers to her new phone.

Today has been really good. We went and visited with my Stake President this morning to finish my living ordinance recommend. He talked to us for 40 minutes or so and just told us stories and gave us counsel, it was really good. He made us promise that one of our children could marry one of his grand-children. :) After the interview Brittney just came with my to church, which was great. The testimonies were good, and in Priesthood we had a really good lesson our duty to minister to those around us as Priesthood holders. It was powerful. We also went to see Brittney's Bishop this afternoon, and we'll go see her Stake President next Sunday. Check, check, check! :) The Church is true!

We're super excited to get married. We found out yesterday that we can start moving into our new apartment this Friday, so that will give us plenty of time to get things settled before we leave. Life is good! We love you all!
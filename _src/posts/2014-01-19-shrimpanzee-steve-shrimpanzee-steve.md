    Title: Shrimpanzee! Steve! Shrimpanzee! Steve!
    Date: 2014-01-19T11:25:05
    Tags: Weekly Letters

Hey Family, hope you're all doing well. :)

We've had a pretty good week out here. School is slowly picking up pace for me, but I think it will be very manageable this semester, so that's good. :) Brittney's class has been going well too, she's excited for the three-day weekend. :)

Monday night we had FHE with Brittney's dad. We had a good time playing rummikub for the activity, and then Mike and his roommate came over afterwards to talk for a bit. Brittney's Dad left Tuesday morning, which was kind of sad, we enjoyed having him here for the weekend.

Wednesday we started exercising again! Brittney went walking with Kristine, and I played racquetball with Michael and his friend, Jacob. It was awesome to play again! But I've been super-duper sore ever since. Totally worth it. :)

Thursday was pretty routine, we made a delicious salad for dinner and then went walking/running. Afterwards, we gave Mike a ride home from campus and then I just did some homework. Friday night we just watched the new White Collar. Holy Smokes. :)

Saturday was a pretty calm day. I got up early and started working on some homework. Brittney decided she is going to make a baby blanket/quilt, so she went shopping for fabric while I played racquetball again with Mike and Nate. We played two grueling games with all three of us. :) That night we went to Zupas for dinner and then went and saw Cloudy with a Chance of Meatballs 2. (Hence the title. :) ) We thought it was pretty good actually, on par with the first one. There was more of a plot than we expected, and lots of silly puns. :) Have a great week, we love you all!
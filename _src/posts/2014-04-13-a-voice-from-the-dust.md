    Title: A Voice from the Dust
    Date: 2014-04-13T14:59:04
    Tags: Weekly Letters

We're still alive, quit worrying about us. :) Sorry it's been a few weeks, I just haven't taken the time to write. :) We spent last weekend in Park City with Brittney's family and watched conference, and I don't remember why I didn't write the week before that. :)

This week was Brittney's last week teaching! Her school has Spring Break this week, but Brittney won't be going back after that, so she's done! She's excited, but also kind of worried about having enough to do around the house. She hit her due date yesterday, but there is still no sign of baby Draut. Hopefully she decides to make an appearance this week. :)

This was also my last full week of school. We have class Monday and Tuesday this week, but then finals start! I'll start my job at FamilySearch two weeks from tomorrow. I'm stoked. :)

Brittney's family came down to Provo a few days this week before heading home. They dropped Heather off, so she has been staying with us until she finds a place to live. We've enjoyed having her here. Mindy stayed here too while we were in Park City, but we didn't get to see her. :P

Friday night was the Senior Celebration on campus. It's just a big going away party for the seniors, put on by BYUSA I think. They had lots of free food and ice cream, carnival games, board games, and raffles for things that graduating seniors would need. We thought it was funny how many people came with a spouse and a kid or two in tow. :)

Saturday we cleaned the house and ran some errands. It's so nice having a clean home! We've enjoyed being able to relax a bit with school winding down for both of us. We love you all!
    Title: We Be Jammin'!
    Date: 2015-08-30T19:51:08
    Tags: Weekly Letters

Hey Family! Hope you're all doing well. We've had a pretty good week. :) The exciting part of this week was going to meet with Veritas (Ivory Homes' preferred lender) to get pre-approved. I've only had my credit card for ~2 months, so we were really anxious to hear my credit score.

<!-- more -->

We walked in around 4pm on Wednesday, and sat down with the Mortgage officer, Jacque. (Jackie) We got started, and the conversation went something like this:

Jacque: "Ok, so your credit scores came through just fine, I assume we'll be doing a conventional home loan."

Ben: "Really?! The scores came through good for both of us?!"

Jacque: "Yes..."

Brittney: "What were they? The scores?"

She told us what they were, and our jaws dropped. :) Brittney's were still pretty high, but mine were about 20 points higher than even our most optimistic predictions! (After talking to James 2 months ago.) She said my history from student loans probably came back once I had a credit card that started reporting to the credit bureaus again. Yay! We were super grateful. :) 

We were supposed to have our pre-construction meeting on Thursday, but it had to be postponed because Wes was still waiting on some paperwork from the architects. Brittney went back to the design center Thursday afternoon to finalize our color choices. She's really excited. :)

At work this week I've been on call with another guy in Orem. (I've kind of been training him.) Thankfully it hasn't been too hectic.

Brittney's Dad came out this week to drop Spencer off at school. He's already meeting girls and seems pretty happy to be here. We all went out to dinner Thursday night to celebrate Heather's birthday. It was fun, but another reminder of why we don't like to go out to eat with Abby. :P

Saturday was a fun day. We went raspberry picking first thing in the morning with the family down in Payson. Brittney and I picked 12 lbs. (Which is all we could get with the cash we had brought. :)) Afterwards we hung out at Mom and Dads for an our or two, and then headed home. We did some chores and then tried our hand at making raspberry jam. :) It turned out pretty well. (We hope) We made 8 jars, and dirtied about 12,000 pots and towels in the process. :) 

After making the Jam we played with Abby for a bit before heading over to Seven Peaks. It was fun being there, but I was _not_ thrilled about how crowded it was. :) It ended up being pretty fun anyways. :) Abby _loved_ the wave pool. She would hold my hands and walk out until the water was about up to her stomach, and then let the wave hit her and knock her back into my legs. After the water washed over her face she just laughed and laughed and wanted to keep walking into deeper water. It was great. :) 

Love you all!

    Title: Happy Halloween!
    Date: 2014-11-02T13:15:14
    Tags: Weekly Letters

Hey Family!

Sorry its been a while since we last wrote, time to start again. :) We've had a good week. Work has been a little less demanding for me, which has been nice. Abby had a cold last weekend, but she's back to her normal happy self now. She gets cuter every day. We started playing a new game this week where we'll sit her on our bed, and then I'll crawl around on the floor and pop-up in different places. She looks around for me, and then laughs when she sees me. :) 

Brittney had a busy week too. They went tutoring, to visit Grandma, to the doctor, worked on some things for activity days, lots of stuff. Brittney says Abby is sad that there are no more soft yellow leaves to play with on the grass, though she was happy to see the fake ones that we put up in our windows. :)

We've also seen a lot of Michael this week, that's been fun. :) We're excited to have him and Loie over for dinner tonight. I've gone back to working on Simap during my commute. I've been making a lot of progress. We're excited to start using it. (Hopefully soon!) 

Friday at work several of my coworkers brought candy to work. I thought they were just bringing it for the team, but then around 3pm a bunch of families started walking around trick-or-treating at cubicles. I'll have to bring some next year. :) Friday evening we had another couple in our ward over for dinner for Halloween. We had fun visiting with them and getting to know them better.

Today is our first day going to church in our new ward building. We had a trunk-or-treat activity there on Wednesday, the building is beautiful. It's up behind Seven Peaks, so we can see most of the valley from the parking lot. It's lovely. :) Love you all!
    Title: The rains came down...
    Date: 2014-08-10T16:52:28
    Tags: Weekly Letters

Hey Family!

Hope you're all doing well! We've had a pretty good week out here. We've gotten a lot of rain and some cooler temperatures lately, which has been super nice! We're really loving having a nice porch to sit outside on. :)

Abby continues to grow a little more each day. She's getting big! She loves sitting out on the porch with Mom and watching the cars drive by. (Especially motorcycles!) She's almost four months old. On one hand, we can hardly believe it. On the other hand, its only been four months!? :) She's so adorable. We love her. :)

Work has been good for me this week. Still a lot of new things, but it's getting easier. Next iteration I'll be on call for the first time...that should be exciting. :) I'm a little nervous, but looking forward to the experience too. Thankfully one of the senior guys on our team is going to be helping me out.

We've had a good weekend so far. We had a pretty busy Friday planned, but then everything changed, so we just had a relaxing evening at home. I was going to help drive the scouts in our ward up to a campout, but they ended up having enough drivers. We were also going to help babysit for a family in our ward that just had twins, but that fell through as well. So around 3pm, our Friday evening opened wide up. We filled it with playing with Abby, tidying up the house, and White Collar. :)

Saturday was pretty busy. Heather and Brittney went shopping in the morning. Brittney was looking for some clothes for her and Abby for Mindy's wedding, but also for our family pictures that we're going to take at the end of this month. I watched Abby while they did that. That afternoon, I took the truck down to Jiffy Lube to get everything checked out and get a safety/emissions check for registration. I thought for sure the brakes would be in bad shape...but it turns out they're fine! That was a relief. I failed the safety check due to a crack in the windshield though, so I still need to get that fixed. After all that we went grocery shopping and cleaned the house some more.

Brittney's family is coming in to town tonight, they're going to stay with us for a couple of days. They've been on vacation in California and at Lake Powell. We're excited to see them. We were talking earlier about how much fun it has been to see so much of family this summer. :) We love you all!
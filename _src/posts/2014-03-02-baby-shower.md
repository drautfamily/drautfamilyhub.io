    Title: Baby Shower!
    Date: 2014-03-02T20:55:45
    Tags: Weekly Letters, Pictures

Hey Family!

Hope you're all doing well, we had a busy week out here! Yesterday we drove up to Kaysville for Brittney's baby shower. Her Aunt Theresa threw one for her and the family in the area. (I drove up with them, but then did homework. :)) Brittney and Kristine had fun though, their family is really sweet. Brittney is really excited about a dress that one of her aunt's got her, and a baby blessing dress from her mom and Kristine.

Baby shower spoils:

<img class="alignleft" alt="" src="https://lh5.googleusercontent.com/-LgSHaLJGybs/UxPq5kGu-mI/AAAAAAAADTw/0zSu30J5N7s/w789-h592-no/IMG_20140302_193010.jpg" width="284" height="213" /><img class="alignnone" alt="" src="https://lh5.googleusercontent.com/-sLIkoZjbf4A/UxPq-x_XCXI/AAAAAAAADT8/qXHf8qqbs6w/w444-h592-no/IMG_20140302_193022.jpg" width="160" height="213" />

Once we got home from that we were pretty tired, but there was a concert at BYU that a friend of Brittney's gave us tickets to. :) It was a tribute to Frank Sinatra or something. They had part of BYU's Philharmonic Orchestra + The Synthesis Jazz Band + a BYU Alum Jazz Singer (Jack Wood) + Vincent Falcone (Sinatra's Pianist/Conductor), and they just sang a bunch of classic Sinatra songs. It was really good. :)

Brittney was busy with report cards this week, as well as other preparations for parent teacher conferences next week. She's really excited... :) She gets Friday off again though, so she is happy about that. :)

I spent most of my time this week working on Codetrain. (My group project for Software Engineering.) I've been working with another guy on what we're calling the engine, the piece of the website that will actually run people's code. We get to solve a lot of interesting problems, so I'm enjoying that. It's similar to some of the work I did last Summer at FamilySearch. I'm getting excited to start there again in April. :)

Today we went to church, went for a walk, and then had dinner with Kristine before watching the CES devotional tonight. It's been a good day. We're pretty blessed. We love you all!
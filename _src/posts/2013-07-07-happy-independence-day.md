    Title: Happy independence day!
    Date: 2013-07-07T13:23:00
    Tags: Weekly Letters

Hey Family,

What a great week. :) Work has been going well. I'm still working on making some changes to our automatic alert system. I really enjoy seeing how a large company deals with issues that we handled manually at work at BYU. It's neat to see all the different tools that are out there, and what they're capable of.

Brittney started tutoring this week. The parents of one of her students last year asked her if she would be willing to tutor their son this summer, and it's going well so far. They are super nice people, and Brittney loves their son. She's excited about it. She also has some professional development classes on various topics that are going to be starting soon that she is also looking forward to.

Once we were both home Wednesday afternoon, we packed up some things and headed up to Logan, which most of you know, because you were there. :) We stayed up there til last night. We had a great time spending time with family, tubing down the river, and just relaxing. :) The fireworks were great too, happy 4th!

Church this morning was really good. I really enjoyed our Priesthood lesson. The topic was "My soul delighteth in _____." We spent just a few minutes talking about how we would fill in the blank, and then looking at ways that the prophets have filled in the blank. Like Nephi, whose soul delighted in the scriptures, in the covenants of the Lord, and in testifying of Christ. We shared some thoughts on things that we felt we should delight in afterwards, like personally studying the scriptures, or rendering service. It was really good, we all wanted to be better. :) The Gospel is true. We love you all!
    Title: Mountain Biking!
    Date: 2013-05-26T14:49:06
    Tags: Weekly Letters

Yes. Mountain Biking. Definitely one of the most exciting parts of the week. :) Yesterday we headed up to Lambert Park. It's an area in the foothills of Alpine with 16 miles of biking trails for all skill levels, and it is awesome. :) And we forgot to take any pictures... oops. But! I did find a <a href="http://www.youtube.com/watch?v=6o4AIB5b-e8" target="_blank">YouTube video</a> of the trails we rode on, so you can see that. :) Brittney, Kristine, Chris and I all went.

After that Brittney had a big combined ward activity, so I helped her get things ready for that. We took the food to the park and then helped fill up water balloons until the barbecue started. It was great, we played kickball with kiddie pools full of water balloons for bases. Very hard to focus on kickball. :) We also tried <a href="http://en.wikipedia.org/wiki/Slacklining" target="_blank">slacklining</a>, which was fun too!

After the activity we went to see Escape from Planet Earth at the dollar theater. It was cute, but we don't think we'll see it again. Not much there besides a couple funny parts. :) Oh well. We also started watching White Collar this weekend, why didn't you guys ever tell me about that show before?? ;)

We're super excited about the three-day weekend. We're hoping to find a bed and some other furniture on Monday during the sales, so wish us luck. :)

The rest of the week was good. Brittney has been having a really good time with her class getting ready for a play. They're putting on their own rendition of Hansel and Gretel, and they kids are loving it so far. Work has been good for me, I finished my last project so they started me on a new one this week. I'm enjoying it and learning a lot, so it's good. :)

Church today was good, the talks were all centered around Mosiah 2:17, there were lots of good insights. My favorite: Serving others increases our capacity to love. Love you all!
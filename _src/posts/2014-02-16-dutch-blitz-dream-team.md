    Title: Dutch Blitz Dream Team!
    Date: 2014-02-16T21:01:20
    Tags: Weekly Letters

Oh man. Me and Grandma. We were like the bulldozer that flattens an anthill, like the tidal wave that destroys a city, like the avalanche that consumes a mountainside.

We were unstoppable.

It didn't matter how fast they were, we were faster. Cards flew around the table in a flurry of colors and numbers, a jumbled mess to the others. But to us, everything was clear. The numbers climbed, the stacks grew, the opposition dwindled, and the triumphant shout came: DUTCH BLITZ!

Wails of despair and sadness echoed from around the table.

The Dream Team had vanquished. Again.
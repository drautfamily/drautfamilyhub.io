    Title: No Title
    Date: 2013-05-19T17:09:55
    Tags: Weekly Letters, Pictures

Hey Family! We hope you're all doing well! This week has been great. It started out Monday with a bonfire for FHE. :) Brittney's ward was having a combined activity at South Fork Park up Provo Canyon. We did a tiny bit of hiking, had a bonfire, saw a fire spinning performance, and just had a good time. :)

<a href="https://lh5.googleusercontent.com/-YEO6CdWJr08/UZGyVbiqOuI/AAAAAAAABHo/w-GilpcXSFY/w958-h719-no/IMG_20130513_200934.jpg"><img class="aligncenter" alt="" src="https://lh5.googleusercontent.com/-YEO6CdWJr08/UZGyVbiqOuI/AAAAAAAABHo/w-GilpcXSFY/w958-h719-no/IMG_20130513_200934.jpg" width="207" height="155" /></a>

We went to the Temple on Tuesday night, which was good. It felt like it had been a while. Wednesday evening we just had dinner and relaxed. We watched some Studio C and caught up on Granite Flats. We're not sure what to do now that most of the wedding stuff is done...we think... :) Thursday evening I helped some friends in the ward get a refrigerator with my truck, and then Friday Brittney headed home to MI for the weekend. She comes back tonight though, not that I've been waiting for it or anything. ;)

Work this week has been good for both of us, although Brittney's kids are losing control of themselves since it's the end of the year. :) I'm just about finished with the project that they put me on initially at FamilySearch, so hopefully I'll get to join in on the bigger team projects next week, we'll see. :) I'm enjoying it though. Though I don't really enjoy the commute from Provo, I really enjoy the days that I get to work up in SLC with everyone else.

This weekend has gone pretty fast. I spent the vast majority of it working on Greebles. :) You'll have to read my other blog for more info on that though. :) Chris, Kristine and I were all going to go mountain biking yesterday, but the weather was nasty, and Chris had a ton of homework, so we didn't. I decided whatever happens next weekend though, we're going. :)

Church today was good, we talked a lot about service, and purity. I really liked the Elder's Quorum lesson, all about how President Snow taught the members of the church to live in such a way that they could invite the Lord to search their hearts, and not be ashamed. It was good stuff. :) We love you all!
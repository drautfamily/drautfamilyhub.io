    Title: Mom and Dad's Visit
    Date: 2015-05-05T12:22:06
    Tags: Weekly Letters, Pictures

Hey Family, hope you've all had a good week! We're glad you're ok, Tiffany. :) We've had a pretty good week. It has been a lot of fun spending time with Mom and Dad. We like the new house and are excited to visit! :) 

<!-- more -->

Monday was pretty normal. A regular day at work and home, plus FHE in the evening. Brittney took Abby to the duck pond too, they had a good day. :)

![Abby likes hats](https://lh3.googleusercontent.com/-HP_D5g6F1ao/VT5IcS-V2CI/AAAAAAAAAY0/KLqg8plvXJU/w651-h872-no/2015-04-27.jpg)

![Abby likes her hat especially](https://lh3.googleusercontent.com/-VRhMvgI2PYE/VT5LtOLKD_I/AAAAAAAAAZA/4_bOQTq7frM/w651-h872-no/2015-04-27.jpg)

![At the duck pond](https://lh3.googleusercontent.com/-6ITN5ozb5dk/VT5oqt46l8I/AAAAAAAAAZg/ftC2C72tEYA/w651-h872-no/2015-04-27.jpg)

![Ducklings](https://lh3.googleusercontent.com/-zaA93ix8adc/VT5pHjEGNvI/AAAAAAAAAZo/tqV-4MjIKzc/w651-h872-no/2015-04-27.jpg)

Tuesday Brittney saw Mom and Dad during the day, and then tutored in the afternoon. Her and Abby also when to the park:

![At the park](https://lh3.googleusercontent.com/-NNZY4cby8TQ/VT-d_Ln-9dI/AAAAAAAAAaI/ujqk_a_fz-s/w651-h872-no/2015-04-28.jpg)

Tuesday evening we met up with Mom and Dad to see the new house! It was good to see them again, and lots of fun looking around the home. It is really similar to our Michigan home in a few ways, which was surprising. :) After seeing the home we went to In-n-Out to get a snack and visit some more. Abby started getting pretty cranky though, so we didn't stay too long.

Wednesday was a regular day. Mom and Dad came over in the evening with the Paris' for a bit, they stayed until Brittney came home from Young Women's. Here's another picture of Abby, just because:

![She loves Mandarin Oranges](https://lh3.googleusercontent.com/-VoPkiYUz-mo/VUFY4CpMS0I/AAAAAAAAAac/Rz8u7qOfysw/w651-h872-no/2015-04-29.jpg)

Thursday was a long day. We've been falling behind this sprint at work, so I stayed late to try and help us catch up. I got home late and went straight to sleep. :)

Friday evening we went out to California Pizza Kitchen for dinner with Mom and Dad and Mindy and Michael. Then we walked around the new R.C. Willey and entered their drawings for free furniture. :) After that we said goodbye to Mom and Dad, and went home. 

![Abby helping Mom in the kitchen](https://lh3.googleusercontent.com/-JIjGAZmUo9I/VUP14gieuFI/AAAAAAAAAbQ/dmgfbLPWwDY/w651-h872-no/2015-05-01.jpg)

Saturday was busy. I helped clean our chapel, and then watched Abby while Brittney went to a couple of ward activities with Mindy. After lunch, we drove up to Kaysville for Dan Barlow's Masters Graduation open house. (He is Brittney's cousin) After the open house, we stopped in Lehi on the way home to see the Tulip festival! It was beautiful. Abby had a lot of fun playing with the rocks, smelling the flowers, and watching the fish and waterfalls. :) I shared an album with those pictures with everyone on Google+, so let me know if you didn't see it.

![Smelling Flowers](https://lh6.googleusercontent.com/-B_MaZ3989WE/VUV7upEwPuI/AAAAAAAARDE/9JDyiKUQxHg/w850-h638-no/IMG_20150502_173007-ANIMATION.gif)

Sunday was pretty quiet. Brittney went to ward council in the morning while I watched Abby, Church was good, (though Abby was a bit rowdy!) and dinner was fun. Love you all!

![Payson Temple Open House from last week](https://lh6.googleusercontent.com/-XMSR6Yq9Y7E/VTwvKAurVwI/AAAAAAAAQzk/SGGBF6e-Ri8/w654-h872-no/IMG_20150425_175155.jpg)
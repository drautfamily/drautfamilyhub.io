    Title: Summer, School, Star Wars, and Scale
    Date: 2013-05-05T15:14:10
    Tags: Weekly Letters

This week has flown by! I started my internship, we finished sending out announcements, finally found a bike for Brittney, it's been busy!

I'm loving my internship at FamilySearch so far, it's a great place to be. I headed up to SLC on Monday for orientation and got started right after that. I'm officially part of the Developer Platform Tools team, along with about 8 other people. Our job is to make sure that the tools and infrastructure are there and stable so that the developers can do their jobs. It's amazing to see how big everything is and how it works, it's amazing to me that it <em>does</em> work!

My first project is customizing a reporting tool to give us accurate report information on our equipment usage. I've really been enjoying it, production on this scale is a whole new world. :) We had a big meeting on Friday in the Church Office Building for all members of FamilySearch to review the business goals, among other things. The goal to have a single worldwide public family tree has been a goal for a long time, and this was the first meeting after having released it. It was so cool to hear everyone's experiences and joy at having achieved it. :)

Brittney's class is getting ready for the standardized tests, after which things will relax a little bit. Both her and her students are looking forward to that. :) Just a few more weeks until summer break! We volunteered at the MTC again on Friday night. We were taught by two sisters, one of whom was in Brittney's ward in Japan years ago!

Saturday morning we went to the Temple, and then went bicycle shopping Saturday afternoon again. Kristine came too; she's a good luck charm or something because they both found bikes they liked this time! I was really tempted to buy a relatively cheap road bike for commuting too, but Brittney saved me. :) They both got their bikes and then we had dinner and went for a ride down the river trail. Since it was Star Wars day, we of course had to watch that too. :) Saturday night I also made a bit of progress on Greebles, check it out <a href="http://drautb.wordpress.com/2013/05/05/progress/" target="_blank">here</a> if you're interested.

Church today was good. Lots of paperwork and getting-to-know-you stuff going on. In Elder's quorum we had a good lesson on conversion, and there were lots of good testimonies of the Atonement in sacrament meeting. The church is true! We love you!
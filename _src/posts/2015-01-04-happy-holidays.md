    Title: Happy Holidays!
    Date: 2015-01-04T19:23:00
    Tags: Weekly Letters, Pictures

Happy Holidays everyone! Hope you've all had a good vacation, we have. Sorry we haven't written much lately.

<!-- more -->

Brittney's family flew in on Christmas day. They arrived at our home around noon, and everyone rushed in to see Abby. :) We hung out for a little bit and just visited, and then we all drove up to Eden. Brittney's parents found a good deal on a cabin rental for the week, so we all stayed there. We had a good time just being together up there, playing games, etc. The weather was pretty bad at first, so I just worked a partial day from the cabin on Friday. Saturday morning we got up early and headed to Snow Basin for the day. I decided I would snowboard, so I took a beginner's lesson and spent the rest of the day practicing. I was sore by the end, but it was a lot of fun. Brittney had a good time too, she hasn't been skiing in almost 10 years.

We had planned to stay til Thursday morning when Brittney's family left, but Abby was having a really hard time at the cabin. She was not sleeping well at all, and Brittney was starting to feel sick, so her parents drove them back to Provo on Tuesday while I was at work. They recovered that night and Wednesday, which was really good. Wednesday night I came home and asked if she wanted to drive back up to spend New Years's with her family. We got back to the cabin around 8:00, and just walked in. Kristine knew we were coming, but everyone else was super excited! It was fun to surprise them.

Thursday was a pretty relaxing day. I had the day off, so we just cleaned the house a bit, ran some errands, etc. We also decided to take Abby sledding at Rock Canyon park. We weren't there very long, but she seemed to enjoy it. :)

The rest of the week has been pretty calm. Abby keeps growing up. She can stand on her own, but she still likes to crawl everywhere. :) She's in love with Cheerios, and loves to spit. :) Love you all!

![](https://lh4.googleusercontent.com/CVZealthK9wMFgaB5MDl9qJWFdWTkW66jrofC5UqqAI=w529-h397)
![](https://lh4.googleusercontent.com/-PkqfFhHvMLY/VKnJ7F_JMmI/AAAAAAAAPnU/rJuXKlhM-z4/w298-h397-no/IMG_20150104_120334.jpg)

    Title: Missed a week... Oops :)
    Date: 2013-07-28T12:59:55
    Tags: Weekly Letters, Pictures

Hey Family, we hope you're all well. We had a great time visiting last weekend, it was wonderful to see everyone. The trip back was surreal, we made it safe though, thank goodness for backup drivers, snacks and audio books. :) Here's a few pictures from the way out. The sunrise was from Rawlins, just before we took off that morning; it was incredible.

<a href="https://lh3.googleusercontent.com/-LXXDxqW87Zg/UelLw0Gf_NI/AAAAAAAAB88/yaNQsfRz1YQ/w755-h566-no/IMG_20130718_055334.jpg"><img class="alignleft" alt="" src="https://lh3.googleusercontent.com/-LXXDxqW87Zg/UelLw0Gf_NI/AAAAAAAAB88/yaNQsfRz1YQ/w755-h566-no/IMG_20130718_055334.jpg" width="272" height="204" /></a> <a href="http://benandbrittney.files.wordpress.com/2013/07/img_20130717_180143.jpg"><img class="wp-image-95 alignleft" alt="IMG_20130717_180143" src="http://benandbrittney.files.wordpress.com/2013/07/img_20130717_180143.jpg?w=640" width="276" height="207" /></a>

This week has been great, it has gone by pretty quickly. I went back to work on Tuesday, but then I had Wednesday off for pioneer day. Brittney and I ran the temple  to temple 5k that a local stake organized, and then just relaxed most of the day. We hung out with Kristine, I cleaned off those hard drives I brought home, and we went and saw Epic at the dollar theater. :) It was good, but the Croods is still my favorite of this summer. :)

We also went to the Pioneer village down the street to check out their <a href="https://plus.google.com/photos/110136320936175613669/albums/5905753533950640353?authkey=CPWR15KA36mDXA" target="_blank">Pioneer Day festivities.</a> That was fun too. :)

Work is going well, each day I get a little more comfortable with things. I'm enjoying the projects they're putting me on. Brittney is enjoying tutoring, and savoring her last bit of time off. She goes back to school in about two weeks.

This weekend has been good. We volunteered at the MTC again on Friday night, which was fun. After that we ran a couple of errands and then watched White Collar. Saturday was busy. Two families in our ward were moving out that morning, so I helped out with those. Then around noon we headed up Provo Canyon with Kristine and Chris. The four of us took the Provo River Trail all the way from the trailhead at Vivian Park, down to Utah lake. Brittney and I had our bikes, and Chris and Kristine had longboards. :) It only took us about two hours all together. :) It was fun to see the whole trail, and we had worked up quite an appetite, so we decided to go to California Pizza Kitchen for dinner. Yum. :)

Church today was good. A lot of changes happened last week while we were gone! We have a new counselor in the Bishopric and a new Elder's Quorum President. The talks and lessons were good though. We love you all!
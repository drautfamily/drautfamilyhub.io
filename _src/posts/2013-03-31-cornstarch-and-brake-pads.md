    Title: Cornstarch and Brake Pads
    Date: 2013-03-31T17:54:05
    Tags: Weekly Letters, Pictures

I hope you all don't mind reading this rather than emails, I wanted to give it a shot. :)

Brittney was home in MI for the week as you know, I drove her to the airport Monday morning an picked her up Friday night. In between, the week went really fast with school and work keeping me busy. Brittney really enjoyed being home for a few days and having the bridal shower, she hopes you had good time too mom!

This week I decided what I would do for my final graphics project in 455, and I'm pretty stoked about it. :) We had a choice to either make a game, a <a href="http://en.wikipedia.org/wiki/Raytracer">raytracer</a>, or a hyperdimensional data display. (Displaying some kind of chart of more than 3 variables in an understandable way.) I was having a hard time deciding which one I wanted to do, until I got to talking with Nozomu. 20 minutes later, I had an idea for Calvin and Hobbes game that I'm fairly confident I could pump out in two weeks. :) It's basically just going to be a "survive as long as you can and rack up points" kind of game in which you defend Calvin's snow fort from the attacking zombie snowmen. Pretty sweet, I know. :) I'll keep you posted.

This weekend has been crazy busy. I picked Brittney up Friday night at the airport and took her home. Saturday morning we drove up to Kaysville so that one of her old mission companions could take some more engagement pictures for us. She's also going to design our announcement. We did that for an hour and were back in Provo by about 12:30. Brittney's family, (who left MI Friday morning to drive out here) arrived at about 2:15. We unloaded their car, and then headed down to Spanish fork for the Festival of Colors.

<a style="color:#ff4b33;" href="https://lh4.googleusercontent.com/-xtW0c1DWjDU/UVfDdfm2qjI/AAAAAAAAAqA/Gbv9AwmqrTY/s902/IMG_20130330_150340.jpg"><img class="alignnone" alt="" src="https://lh4.googleusercontent.com/-xtW0c1DWjDU/UVfDdfm2qjI/AAAAAAAAAqA/Gbv9AwmqrTY/s902/IMG_20130330_150340.jpg" width="227" height="171" /></a>          <a href="https://lh3.googleusercontent.com/-xDto31wN6HA/UVfDdWtxTmI/AAAAAAAAAqY/gq2tsXU9zuI/s902/IMG_20130330_162217.jpg"><img class="alignnone" alt="" src="https://lh3.googleusercontent.com/-xDto31wN6HA/UVfDdWtxTmI/AAAAAAAAAqY/gq2tsXU9zuI/s902/IMG_20130330_162217.jpg" width="227" height="171" /></a>

After everyone cleaned up back in Provo, we headed out to Cafe Rio to get dinner and say hi to Robert. (He had to work 7pm to midnight) The food was great, but we went to the wrong Cafe Rio! So we headed over to the right one and said hi. :) After that we played a game, loaded questions, before everyone headed to bed around 10:30.

Church was awesome today, we had a great Easter program. I taught one of the Sunday School classes too, which went ok. It was on the gathering of Israel. I had a neat thought while I was thinking about it. Christ is gathering Israel as a whole, but He also gathers us individually. I hadn't thought about that before. :) He lives, Happy Easter!
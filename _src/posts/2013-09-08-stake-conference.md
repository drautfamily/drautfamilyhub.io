    Title: Stake Conference!
    Date: 2013-09-08T13:49:23
    Tags: Weekly Letters

Hey Family,

This has been kind of a scary week, for me at least, with school starting. :) Brittney's class is doing well and she loves them. Classes officially started on Tuesday, but all of mine are on MWF, so I just worked all day. On Wednesday I had every single class: Intro to AI, Programming Languages, French History I, Machine Learning, and French History II. I love the first day when you get to see all the cool things you're going to study. :) After my last class finished, I did a little bit of homework, and then we celebrated Brittney's birthday. :)

The rest of the week was pretty routine. Brittney taught, I went to work and class, we ate dinner, I did homework, we did it again. She started coming down with a cold on Friday, so we had a pretty low-key weekend. Although we did see <a href="http://www.youtube.com/watch?v=aKIlVsm6ypc">Turbo</a> at the dollar theatre. It was pretty good, it made me laugh. :) I thought there was a good message at the end of it too. :)

This weekend was also stake conference for us. Elder Gifford Nielsen was the visiting authority. There was a lot said about strengthening not just the Family, but also one another, the larger human family. A lot about the Temple too, and how important it is to worship there regularly. It was an inspiring weekend. :) We love you! The Gospel is true!
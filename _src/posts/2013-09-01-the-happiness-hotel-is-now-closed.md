    Title: The Happiness Hotel is now closed
    Date: 2013-09-01T13:13:57
    Tags: Weekly Letters

Hey Family!

Well, it's official. The Happiness Hotel closed this week, or rather, went out of business. Our last tenant left Thursday morning, and no one else has come, so we can't afford to keep it running. Oh well. :) Michael moved out Monday morning, and then Brittney's Dad came into town and stayed with us Wednesday night. We went out for a delicious dinner at the Brick Oven. :)

This was Brittney's second week of school. She's happy that a routine is started to emerge. :) Her ESL endorsement class is going to start up next week on Tuesday nights, and she'll continue tutoring on Thursdays, which she is really enjoying.

Wednesday was my last day at FamilySearch. The team took me out out to lunch at Zupas to celebrate! We had a good time talking. That afternoon before I left, Bob, our team manager, came and expressed a lot of appreciation for the work I had done. We talked for a bit, and he said that he and the team would love to have me back if I was interested later on, so I was like heck yes I'm interested! (Maybe not quite in those words. :)) Anyways, it was all just verbal, but I was really glad to hear that they'd like to have me back! So things ended well there. :)

We decided to get away for a camping trip before school started, (finally!) on Friday night. We headed up to a campsite on a mountain behind Squaw Peak that we knew of and grilled hot dogs and just enjoyed being out. :) We noticed some storm clouds and lightning on the horizon that looked like it was getting closer, so we decided around 9:30 pm to move camp to a lower campsite closer to the car, just in case. We rolled the tent up with sleeping bags still inside, and carried it all a quarter mile down the trail. :) The storm thankfully didn't make it to us, so we were fine, just a little tired after trying to sleep on the rocky ground!

Church today was good, there were good testimonies and lessons. I came across D&amp;C 88:123-124 in Sunday School. Look it up. It's a good one. :) The Gospel is true! Love you all!
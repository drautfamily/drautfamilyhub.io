    Title: More Lazer Tag!
    Date: 2014-08-17T20:11:46
    Tags: Weekly Letters

Hey Family!

Hope you're all doing well, we've had a great week out here. :) Brittney's family came in last Sunday evening and stayed till Thursday morning. It was fun to have them here visiting.

Monday we celebrated my birthday. After I came home from work we had steak shiskebabs for dinner, followed by cake, ice cream, and presents. Dave and Chris, a couple of my friends from work at BYU, came over too. After all the food we played a couple games of lazer tag. Remember those orange and green ones that Michael and I had when we were younger? Well, now we have twelve of them in total, enough to play with everybody. :) That was Brittney's gift to me. :) I also got a couple of computer books that I'm looking forward to reading.

On Tuesday we celebrated Heather's birthday with cake and ice cream again. Her birthday is on the 27th of August. Then we just relaxed and watched White Collar with Brittney's family. Wednesday was our last evening all together before they went home, so we went up to campus to play a few more rounds of lazer tag. Also very fun. There were some fun hiding spots. :)

The rest of the week we've just been taking it easy, trying to get back into our routine. Abby has been sleeping pretty well most nights, which has been nice. She is getting so big! This weekend we went shopping for clothe for family pictures and Mindy's wedding. We also cleaned the house a bit and planned out our week. Sometimes it feels like we have too much going on! (Though I'm sure no one else knows what that feels like.. :))

Church today was good. I subbed for one of our Gospel Doctrine teachers. The lesson went pretty well, everyone likes to talk about Proverbs. :) Tonight has been fun too. Heather, Kristine, and Michael all came over for dinner. The meal was followed by more ice cream (of course!) and Dutch Blitz. It was fun visiting with everyone. Love you all!
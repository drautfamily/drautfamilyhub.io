    Title: Holy Snow Batman!
    Date: 2013-12-08T16:46:59
    Tags: Weekly Letters

Hey Family, hope you're all doing well!

Utah got nailed with snow this week, (just like most of the country) so that has been fun. :) We've had probably 8 or 9 inches so far, it's beautiful. For a couple of days we were in the single digits too, thankfully it has warmed up a little bit.

I've been swamped with final papers and projects this week. The last day of classes is Thursday, and I still have a bunch to do. Wish me luck. :) School has been good for Brittney, she has nothing to report. :)

Friday night we went to Divine Comedy's end-of-semester show with a couple of friends. It was pretty good, they have a new crew though that has their own style. After the show we went to the Cocoa Bean for hot chocolate. It was delicious. :)

I spent most of Saturday doing more homework. Brittney went shopping with Kristine for some things, and then we just relaxed in the evening. We ran a few more errands so Brittney could finish a craft project she was working on, and then we watched the Incredibles.

Today has been good, it's cold but the sun is shining. :) Brittney tells me the baby has been kicking more, but I always miss it. :( Love you all!
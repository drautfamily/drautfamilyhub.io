    Title: Garage Sales
    Date: 2015-08-23T19:45:49
    Tags: Weekly Letters

Hey Family!

Hope you've all had a good week! Yesterday was probably the highlight of the week for Abby. We went looking at garage sales yesterday morning to find a booster seat for Abby, and we found much more. :) We did find a booster seat. We also found a slide, a toddler pushcart, and some toy cars that she liked to play with. All for less than $15! :) Brittney was pretty excited.

<!-- more -->

The rest of the week was good too. It feels like we've been doing a lot to get ready for the new house. We had a pre-Pre-Construction meeting with Wes to just help get things ready for the Pre-Construction meeting, which we'll have this Thursday. Once we sign the paperwork at the Pre-Con. meeting, they'll start digging the hole for our house in about two months. (November-ish) From that point, it's about 3-3 1/2 months before the home will be done.

We also went and looked at a couple of nearly finished Palermos this week. We still liked it walking through, so that was a relief. :) One also had similar color choices as ours, and we liked they way it looked, so that was nice too. We're excited for all the space! And to be close to Mom and Dad.

This week and next we'll be meeting with lenders again to see which one we want to use. I'm still nervous that I won't have a credit score, even though I have a credit card now. :)

Work has been pretty good this week. I've been learning some new stuff, which has been fun. Sometimes I forget how much I enjoy learning new things. Brittney has had a good week too, she's getting really excited for the house too. She hasn't been feeling sick this week either, which has been nice. We have the ultrasound appointment coming up on Sep. 15th. We both think it will be a boy. :)

Abby has been learning lots of new words and animal sounds. She's been saying "MaMa" and "DaDa" more than anything else though. :) Love you all!

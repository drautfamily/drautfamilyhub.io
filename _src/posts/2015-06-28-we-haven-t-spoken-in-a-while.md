    Title: We haven't spoken in a while...
    Date: 2015-06-28T13:40:50
    Tags: Weekly Letters

Wow, it's almost been a month since our last letter. Sorry Tiffany, Trevor, and Grandma! It feels like our pace of life just doubled. :P Let's start with just this week.

<!-- more -->

Spencer and Joe were out here this week. (Joe had two swim camps at BYU, and Spencer was just tagging along) It felt like we were hosting a Sunday dinner every night! :) It was fun having them out here though. We played a little laser tag, watched some movies, and just hung out. I took them to the airport yesterday afternoon to fly home. 

We had a couple from our ward over for dinner on Tuesday night. It was fun to visit and play games with them. Wednesday night Brittney went to the Rec. center to swim for young women's, which she enjoyed. Spencer and Joe went too to just to swim I think, but we teased them about wanting to mingle with the young women. :) 

Friday night we met with Jenny (our agent) and Wes (Ivory's agent) at the design center to start getting a feel for the kinds of different things we'll be choosing on our home. We enjoyed learning more about the different options.

I don't know who knows what anymore, so I'll recap. :) We've backed out of the first lot that we put money down on. It just wasn't going to be good if we stuck with it, so we left. We put money on a new lot, one street north of Mom and Dad's new house. :) The floor plan we're putting there ([Palermo][1]) is a little smaller than the previous one, but this builder's idea of "standard" is much higher quality. (And we trust this builder more.)

Abby just keeps growing. She has 4 teeth now, and her grin is so cute! She is trying to talk more and more, and now she gets frustrated when we understand her signs, but not what she tries to say. :P 

That's about all I can think of right now. Love you all!

[1]: https://www.ivoryhomes.com/?mID=8&homedesignID=271
    Title: It's been a while...How are you?
    Date: 2015-12-06T19:58:13
    Tags: Weekly Letters

Hey Family! Sorry it's been so long since we've written. It's been a bit busy for us lately. We've had a wonderful Thanksgiving vacation though. It was awesome to spend so much time with the family. (Especially with Tiffany and Trevor being out here!)

<!-- more -->

Let me explain what we have been doing the last month. No, there is too much to explain. Let me sum up. :)

Grad School - Ben _finally_ submitted his Masters application to the University of Utah yesterday! (Saturday) Now we wait. I'm pretty excited about it. I hope I get in. I decided to apply for the Thesis program after all. We'll see if I make it in.

House - They've started construction on our home! When we saw it yesterday, they had poured the basement walls, put in the window wells, and then backfilled the dirt around the outside of the house. It looked like they were still working on the underground plumbing. We can't wait for them to finish!

Abby - She has been talking more and more! She knows her shapes, colors, a couple of primary songs, and lots of other things that we don't remember teaching her! She's a smart cookie! She's enjoying nursery too, though she doesn't like saying good bye to Mom and Dad.

Brittney has been looking for reasonable ways that she can earn points towards her teaching license renewal while still being home to take care of kids. She heard about some free online classes from another Mom/teacher in the ward, and decided to check them out. Her first one starts on Jan 14th. She's excited. :) 

Love you all!


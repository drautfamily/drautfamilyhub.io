    Title: 2 More Months!
    Date: 2013-04-14T16:44:34
    Tags: Weekly Letters

Hey Family!

Classes are over! Ok, not until Tuesday actually, but I'm pretty much done. :) All of my final projects and papers were due last week, so finals are pretty much all I have left! Hooray! Brittney has 8 more weeks until school is out, she's hanging in there. :)

All I remember from school this week is working on my Calvin and Hobbes game: <a href="http://drautb.wordpress.com/2013/04/12/snowman-apocalypse/" target="_blank">Snowman Apocalypse</a>. I finished it up just in time on Friday. I'll present it Monday in class, and then that will wrap up Computer Graphics.

Brittney and I went to the Temple Friday night to finish the sealings for the family names Dad sent me last year. It feels good to have finished those. After the Temple we went and volunteered at the TRC in the MTC. We had a good time there, it was pretty strange going back and seeing how things have changed.

Saturday was a super busy day. Brittney's stake has been doing a wellness thing all semester long, and it culminated yesterday with a Stake 5K around Lindon. Thankfully we're not too sore today. :) We spent the afternoon running errands with Kristine, looking for a bike for Brittney, getting fitted for my tux, and eating dinner with Robert and his friend, Brian. After all that we went to Divine Comedy and then played bananagrams. It was a pretty good day. :)

Church today was good. I was thinking a lot about hope, and how it comes from faith in Christ. The church is true!

P.S. 2 more months till we get married, hence the title. :)
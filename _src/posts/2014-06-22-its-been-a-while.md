    Title: It's been a while...
    Date: 2014-06-22T15:21:19
    Tags: Weekly Letters

Hey Family,

It's been a few weeks since we last wrote...Becky has been really good at reminding me that I need to be more consistent. :)

This week has been good. Yesterday Kristine came over and watched Abby so that Brittney and I could go to the temple and then do our grocery shopping. (We finally did that last sealing I had Dad, for George Hartman and Fannie Draut.) That was fun. Around 1:30 we left for Logan to visit Grandpa. He hadn't seen Abby yet, so we figured they had to meet. :) We didn't tell him we were coming, so hopefully he enjoyed the surprise. :) We visited for about an hour and then he had to go to dinner, so we left. We stopped at In-and-Out on the way home for dinner. :)

Brittney started tutoring Jordan again this week. She's excited to get out of the house some more. :) Wedensday night we did a short double-date with Mindy and Michael P. We went to the duck pond and fed the ducks and then came back and watched an episode of White Collar. (We started watching them over again, Brittney was missing Mozzie.)

On Thursday our friends Luke and Whitney came over for dinner. They were in our ward last summer, but moved out in the Fall. It was fun to see them again, we like them. :) Friday was pretty relaxed. We were both really tired that day, so we went to bed really early.

Work has been going well for me. I'm getting more and more comfortable and making some more contributions. I'm enjoying it. :)

We're looking forward to coming out next week. It's been a while since Christmas. :) We love you all!
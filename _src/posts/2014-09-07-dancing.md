    Title: Dancing!
    Date: 2014-09-07T14:39:05
    Tags: Weekly Letters

Hey Family!

Hope everyone is doing well! :) We've had a good week out here. Monday, for the day off, we just took it easy and relaxed at home. Brittney read a lot, and I worked on Simap. (The food storage app I mentioned last week.) We'v been talking about it a lot this week, mostly just imagining how it would suit our needs perfectly. It's been fun. :)

Brittney had an appointment with the dentist this week, that went well. They also gave her a flower. Awww. :) Tuesday afternoon we also went to the Ogden open house, which was really cool. Wednesday we picked up Mom and Sister Joseph from the airport. We had fun visiting with them on the way back to Provo. Apparently they don't approve of my driving. ;)

Thursday was Brittney's birthday. I made a quiche from our fancy French cooking cookbook for dinner, and then we had cake and ice cream and presents a little later, once everyone was out of class. Brittney was really excited about the Monopoly game I got her. I told her the real gift was that I was willing to play it with her. :)

Friday was pretty quiet. I just had work, we had leftovers for dinner and watched White Collar. It was fun just to be together. Saturday was fun. We got up semi-early and did our budget and cleaned up the house. Then we headed up to Ogden early for Mindy's shower, which was also fun. Abby behaved really well, and then blew out her diaper right as we were leaving. Such a stinker. :) But then she slept the whole way home, so we were happy about that.

After getting back to Provo we did grocery shopping and had some dinner, and then we went Country Swing dancing! According to Brittney, we were the best couple on the floor. The floor was our living room, so we were the only couple on the floor, but it was still fun. :) She also wanted me to say we have some tips for Michael. ;)

Church was good today. There were three baby blessings, so the chapel was full to bustin'! Mom came with us too. We just got home, and I couldn't sleep, so I'm writing this. :) Love you all!
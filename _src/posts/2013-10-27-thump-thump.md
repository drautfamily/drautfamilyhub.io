    Title: Thump Thump
    Date: 2013-10-27T19:25:21
    Tags: Weekly Letters

Hey Family!

Sorry it's been a while. :) This week was good. We went to the Doctor's office on Tuesday for Brittney's checkup, everything is going well! Brittney isn't feeling nauseated as often so she's happy about that. Dr. Glenn said to enjoy it while it lasts. :) We heard the heartbeat again, and this time there was a lot more interference, which he said was the baby moving around. Apparently we have a very active child. :)

School this week was good for both of us. Brittney's week was a little more relaxed. Mine was still pretty busy, but good. I've spent most of my time working on our next project for my machine learning class. I spent most of Saturday working on it too, but I'm almost done! It has been a fun project though, we had to program a <a href="http://en.wikipedia.org/wiki/Artificial_neural_network" target="_blank">neural network</a>. :)

Friday night we went to BYU's showing of Arsenic and Old Lace. It was funny, I don't want to see it again for another 5 years though. :) Way too much dramatic irony. It drives me nuts. :) Brittney enjoyed it too, she had never seen it before.

Saturday morning I helped a family in our ward move while Brittney got things ready for her achievement days activity that afternoon. I worked on my project for the rest of the day until dinner. After dinner we went and saw Planes at the dollar theatre. It was pretty good, a good story, but could have been executed better I think. Hope you're all doing well!
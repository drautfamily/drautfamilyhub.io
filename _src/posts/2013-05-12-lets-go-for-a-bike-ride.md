    Title: Let's go for a bike ride!
    Date: 2013-05-12T18:17:23
    Tags: Weekly Letters, Pictures

Howdy Family!

This week has been fun, a little less busy too which has been nice. We're almost down to one month till the big day!

What do we remember about this week...let's see. We were going to have a bonfire up South Fork Canyon for FHE on Monday, but it was raining and cold, so we played volleyball in the church instead. :) Tuesday night Brittney went out to dinner with her friend Tatianna, she was glad to catch up with her. Wednesday evening Brittney had a meeting for the Drug Free Draper Coalition, but she forgot and carpooled that day, so I spent the evening in Draper with her. :)

Friday night we went to the MTC again, which was good, and then cleaned up her house and made some treats for her bridal shower on Saturday. We attempted to make the Texas sheet cake again, but things went terribly awry this time. That's all we'll say. :)

Saturday was great. I worked some more on Greebles while Brittney had her bridal shower, which went until about noon. From about then to two or two-thirty we (Brittney, Kristine and I) skyped with Heather and the rest of their family. After that Kristine left and Brittney and I went for a bike ride. We were looking for little Eiffel towers to put on the tables at our MI reception, so we wanted to see what kind of knick-knack shops we could find downtown. We didn't find any towers, but we did find a pet shop, a really old antique shop, and a three-room version of DI. Pretty unique. :)

After that we decided to ride along the river trail again. This time we took it all the way out to Utah lake. It was such a beautiful trail! We had a great time.

<a href="https://lh3.googleusercontent.com/-Mwfh7ChkYCI/UY7SBt_owjI/AAAAAAAABFU/fOekvoamF7M/w576-h432-no/IMG_20130511_163346.jpg" target="_blank"><img class="alignnone" alt="" src="https://lh3.googleusercontent.com/-Mwfh7ChkYCI/UY7SBt_owjI/AAAAAAAABFU/fOekvoamF7M/w576-h432-no/IMG_20130511_163346.jpg" width="208" height="155" /></a>        <a href="https://lh6.googleusercontent.com/-ZIEA-kc11sg/UY7SDEQZUiI/AAAAAAAABFg/WoA6ARErIcQ/w576-h432-no/IMG_20130511_163404.jpg" target="_blank"><img class="alignnone" alt="" src="https://lh6.googleusercontent.com/-ZIEA-kc11sg/UY7SDEQZUiI/AAAAAAAABFg/WoA6ARErIcQ/w576-h432-no/IMG_20130511_163404.jpg" width="208" height="155" /></a>

Last night we watched the first half of Gettysburg too, I love the battle of Little Round Top. :) Today has been great too, we just got back from visiting Grandma. We're going to do homemade pizza tonight for dinner on the new pizza stone that Brittney got at her bridal shower yesterday. :)

Brittney came to my ward today for church so that we could go visit Grandma afterwards.  It was fun to have her there. :) The theme was service today, it was good. We also talked about what our purpose is in life right now in Elder's Quorum. We had some time to ponder and make some goals, it was good. The church is true!

Happy Mother's Day! We love you!
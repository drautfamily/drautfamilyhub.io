    Title: Back from Vacation
    Date: 2015-08-16T19:28:58
    Tags: Weekly Letters, Pictures

Hey Family! Sorry it's been a while again, life has been busy! Vacation in Michigan was great. We had a wonderful time with Brittney's family. We went boating, went to the Detroit Zoo, watch White Collar, and Brittney and I cut a mean rug at a wedding reception. :) We miss dancing. :) 

The flight out there was awesome. We ended up in first class on a direct flight, and Abby slept most of the way while we enjoyed our meals. It was perfect. :) The flight home was a little tougher, but thankfully the flight from Cincinnati to SLC was only 2/3 full, so we were able to spread out on our own row in the back and let Abby run around a bit. (We were so grateful!)

<!-- more -->

Abby especially loved being around their dog. Every morning when she woke up, the first thing she would say: "Doggy? Doggy?" Adorable. :) Coming back to real life has been a bit tough, but I think this week will be better. 

For my birthday Tuesday, we decided to just go hang out with Mom and Dad. WE didn't want to face our responsibilities. :) Thanks Tiffany and Grandma for the cards! The rest of the week was pretty uneventful. 

The Alpine Slide yesterday was lots of fun, as was spending the afternoon at Mom and Dad's again. Unfortunately Abby came down with a stomach bug last night and threw up several times. (We went through all 4 sets of crib sheets in one night!) Thankfully, she's still cheerful. She gets really scared when she gets sick though, it's sad. :( This afternoon she has been doing better, so we're hoping she'll do better tonight.

Outside of this week, life is good. We've started our loan application with Ivory's lender. Once we're pre-approved, we'll have a pre-construction meeting and then they'll start digging. We could be moving in February or March! :)

I've started studying for the GRE. I'm registered to take it on Sat. October 10th. Please pray for me. :) Hopefully I can start a Master's program at the University of Utah next Fall. Love you all!

![](https://lh3.googleusercontent.com/V6lTTuvin-Dm93pTNS0BJYJTnbIimiQUOhCBVtDhL4CY=w349-h196-no)

![](https://lh3.googleusercontent.com/dpkbiC3rk6fClMu1OvKMDkehzxuwlpWXQ5Gdw4m746QJ=w1256-h2230-no)

![](https://lh3.googleusercontent.com/x671h2rE8bKTJFCbSIiKMTAirUJ9O15ROu3VIyzhy1Iq=w1256-h2230-no)


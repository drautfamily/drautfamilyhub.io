    Title: The parties are over!
    Date: 2013-06-30T16:10:02
    Tags: Weekly Letters

Hey Family!

What a week! We had such a great time at the open house in Kaysville last night. It was wonderful to see all the friends and family that stopped by. We're so glad that wedding party planning is done though. Now to finish the thank you cards... :)

Most of the week was pretty routine, I worked, Brittney ran errands, practiced Spanish, read, looked for more things to do, etc. She's really excited to start tutoring one of her students from last year this week. :) They actually came to the open house last night, they seemed like wonderful people. Brittney loves them. Kristine has started coming over for lunch every day too, so Brittney really enjoys that.

Work has been going really well for me. I made a lot of progress this week on the two projects they have me working on. I'm hoping to be able to start something new on Monday. We had another department meeting on Wednesday too. It was really neat to hear about things that have been accomplished, and new things that are coming up. :) Brittney had a training meeting on Thursday too, she seemed happy to have another reason to get out. :)

A couple in our ward, the Pendleburys, invited us over for dinner this week. They have a son who is just a couple months old too, we had fun visiting with them. <a href="http://www.melskitchencafe.com/2009/09/crispy-southwest-chicken-wraps.html" target="_blank">Dinner</a> was delicious too. :)

Saturday was super busy. We got up around 7 to make breakfast for Brittney's parents and Kristine. We had scrambled eggs, pancakes, bacon, and cantaloupe. It was wonderful. :) They came around 9:30 and we ate a visited for a bit before heading out to run errands and drive up to Kaysville. We spent the afternoon up there getting everything ready. We were really happy with how everything turned out though, we really enjoyed it.

After cleaning up and driving home, we finally got to sleep around midnight. It made getting up for 9am church a bit difficult, but we made it. :) Church was really good. Sacrament meeting was centered on the power of the Priesthood. We spent a lot of time in D&amp;C 121, which was great. We love this ward. Everyone that talks or teaches a lesson always mentions how "In the Provo Peaks 6th Ward, we do our home and visiting teaching." Everyone is so focused on serving and lifting each other. We love being here and are excited to jump in. :) The Gospel is true. We love you all!
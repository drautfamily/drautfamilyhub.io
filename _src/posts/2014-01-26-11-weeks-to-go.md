    Title: 11 weeks to go!
    Date: 2014-01-26T21:23:29
    Tags: Weekly Letters

Hey Family,

Hope you're all doing well, it was fun to talk on the phone for a bit tonight. We've had a pretty fun week, not too much out of the ordinary. Monday was a holiday, so we spent it cleaning the house and running errands. (Neither of which we had done on Saturday.) We also finally hung up some more pictures in our family room. I also built an extra leaf for our dining table, so we have a bit more room when people come over for dinner. Brittney just found a tablecloth for it yesterday, so now it looks very nice.

Tuesday we went back to school. Michael also helped me pick up the recliner that we had bought on Monday. It's super comfy. :) The rest of the week consisted of classes, work, and homework. Friday night we were going to get some wraps for dinner on campus, but it was closed. :( So we went to The Wall instead. It was pretty good, but not anything to draw us back. Afterwards we came home and watched White Collar with Kristine.

Saturday was a busy day. We went grocery shopping in the morning, and then I spent most of the afternoon on campus working on a project while Brittney worked on the baby quilt she has been making. She is doing a great job, it looks very nice. :) Saturday night Brittney watched a movie with our neighbor, Kristen, and I just did more homework.

Today was really good. We had good church meetings, and then we both went visiting/home teaching, our home teachers came over, and then we had dinner with Michael, Eliza, and Kristine. We had delicious Hawaiian haystacks, and then just visited and played Dutch Blitz. :) Now we're just relaxing. :) Love you all!
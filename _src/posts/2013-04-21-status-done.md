    Title: Status: Done!
    Date: 2013-04-21T13:50:33
    Tags: Weekly Letters

Finals are over! :)

That pretty much sums things up for this week. Tuesday was the last day of classes, Wednesday and Thursday were reading days, and Finals started Friday. I took three finals on Friday, two in the testing center and one online, and then took my last one Saturday night. It feels good to be done. :) We'll see what the gradebooks have to say later...

I also did a lot of planning and architecture design for Greebles this week, I'd really like to finish it this summer. We'll see how it goes, but I feel really good about it. Third times the charm. ;)

Work was pretty routine this week, I worked some more on the intramural program. Hopefully I can get a lot done next week before I leave. We also had a work party on Thursday and took a picture with everyone. Several people are graduating and won't be back next year, we'll miss them.

This weekend was fun. We volunteered at the MTC again Friday night and had a good time. I was pretending to be an investigator again, but we had a few out-of-character moments during our visit, so at the end the Elders asked me: "Wait, you really are a member though right?" Brittney laughed. :) After that we picked up Mindy and Michael and got some pizza for dinner. We all had dinner with Kristine, Robert, and his friend Brian. Robert and Brian left Saturday to go down to Arizona to work for the summer, so it was fun to see them before they left.

After everyone left Brittney and I went to see Life of Pi at the dollar theater  We're still trying to figure it out. :) Some parts I really liked, and some parts were strange, but overall it was ok. :)

Saturday morning we played racquetball, and then I studied some for my last final. We spent the afternoon looking for a bike for Brittney. The one she found that she liked was out of stock in her size, so we'll keep looking. :) We also planted some flowers and did some weeding in her front yard, so it looks much nicer now. I took my last final at 8, and then we watched The Three Musketeers. (The good one from 1993) :)

Church today was good, kind of sad. Our Bishopric was released, so they had each member say a few words in Sacrament Meeting and then instead of Sunday School and Priesthood we just chilled for an hour or so and ate treats while everyone said goodbye. They'll be missed.

On Wednesday night we also went to the Temple. A friend in Brittney's ward, Nicole Browning, was taking out her own endowment, and she had invited us to be there. It was really good, she was so excited to be there. It made me think a little more about how great a blessing the Temple is. The Gospel is true! Love you all!
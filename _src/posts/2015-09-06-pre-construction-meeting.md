    Title: Pre-Construction Meeting!
    Date: 2015-09-06T19:10:21
    Tags: Weekly Letters

Hey Family! What a week this has been! Abby has been throwing _tons_ of temper tantrums. It's getting really old... It looks like she has a few more teeth coming in, and she's had a bit of a cold, so that kind of explains it.

Thursday afternoon we finally had our pre-construction meeting! We finalized all of our design choices, wrote a check, and signed the next set of paperwork to keep moving forward. Ivory is going to submit the plans to the city, and then Wes said they'll probably start digging near the end of November. Our next meeting will be with the Superintendent at the lot just prior to digging. Yay!

<!-- more -->

Work has been good for me. I've been on call this week, so it's been a little hectic sometimes, but overall it has been much milder than previous rotations I've been on. I've been very thankful for that. :) 

I found out this week that my presentation proposal for the SORT conference in October was approved, _and_ that they wanted the rough draft of my slides by Friday. :( That was a little stressful since I can't really hunker down to work on that kind of thing while I'm on call. I ended up finishing them last night, and will submit them tomorrow. The final draft isn't due till the 17th or 18th, so I should be ok. I'm pretty excited about my [topic][1] this year. :) 

I also started the application for admission to the Masters in CS program at the U of U this week. I've been thinking about whether I want to do the "courses only" option, or the Thesis option. The more I think about it, the more excited I get about the Thesis option. I think I'd like to do something related to [program generation][2]. Hopefully I get in. :) 

Brittney has had a pretty good week. It's been hard with Abby being so crabby. Brittney has a lot of little projects she'd like to do, so she got really organized this week and was excited about that. She watched a webinar about organizing which inspired her. :)

Her birthday was Friday! We didn't do too much, just ate some ice cream and watched a movie. We're going to go country dancing at BYU on Tuesday, that's the part she's really excited about. :) We're also going to get here a power sander so that she can refinish a dining table that we got from some friends in the ward.

Love you all!

[1]: https://aws.amazon.com/swf/
[2]: https://en.wikipedia.org/wiki/Automatic_programming



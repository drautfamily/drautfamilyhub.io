    Title: Code Red! Code Red! The baby is mobile!
    Date: 2014-11-23T21:07:21
    Tags: Weekly Letters

That's right, no more leaving Abby to play happily on the floor while we make dinner or do anything else. She started crawling this week! Friday afternoon, on her seven month birthday, she decided that it was worth it to crawl in order to tear down the Jenga tower that Mom had built. :) Now if both of us leave the room, she starts crawling around to find us. :) She's adorable. :)

<!-- more -->

I think that was the highlight of our week. :) It was pretty quiet otherwise. Wednesday I finally got Simap to the point where we could start using it, so we 'launched' it. Brittney has spent a day putting in all of our food storage and noticing bugs, so I still have plenty of work to do on my commute. :)

...

Now it's the end of the day and I'm coming back to finish this up. :) We love you all. Happy Thanksgiving! We're happy for Jared, good choice buddy! We'll miss you guys over the Holidays, but we're excited to come out next Summer!
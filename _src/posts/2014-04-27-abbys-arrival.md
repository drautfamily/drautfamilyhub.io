    Title: Abby's Arrival
    Date: 2014-04-27T20:14:47
    Tags: Weekly Letters, Pictures

Hey Family,

Well, I think everybody knows the big news of this week. :) I'll recap what's been happening though. :)

Last Sunday morning, Brittney woke up at 1am. She woke me up at 2am, and we left for the hospital at 3:30am. We arrived around 4am, Brittney got an epidural around 5am, and Abigail Ann Draut was born at 6:29am on April 21st, 2014. She was 7lbs 11oz, and 19 inches long.

<img class="aligncenter" src="https://lh6.googleusercontent.com/-WtXpo1pfVhc/U1rNetIL57I/AAAAAAAAD6M/No2OnNIDXNY/w1044-h587-no/DSC00011.JPG" alt="" width="381" height="214" />

The first day at the hospital was a little tiring, but good. :) Mindy, Michael, and Kristine were in the waiting room by 7:30 I think. :) It was fun to see them so soon. Robert and Heather came a little later, and then Brittney's Mom flew in that evening. We just hung out in the hospital, talked, slept, and enjoyed looking at Abby. :) That night we had the nursery take care of Abby so we could both get some sleep.

Tuesday morning Grandma came up and held Abby for an hour or so, she seemed so happy to see her. :) Everyone but Michael came back at least for a few minutes. We just slept and talked all day again. We drove home around 5:30pm.

The next few nights were interesting. :) We (or mostly me I guess) would jump up at every sound Abby made to make sure she was ok, so we didn't get much sleep. Then on Thursday Brittney set up a feeding schedule, and since that we've been sleeping much better. :) We make sure to feed her every 2 1/2 hours or so during the day, and our company makes sure she doesn't sleep too long. Then during the night, she sleeps for a 5 or 6 hour stretch, which we like. :)

Brittney's Mom, Robert, Heather, Kristine, Michael and Mindy have all been coming over a lot, they love seeing Abby. :) We've had a lot of fun visiting with them and just being together.

<img class="aligncenter" src="https://lh6.googleusercontent.com/-AKoAsWVz9ik/U110QqSCC5I/AAAAAAAAD_M/sLlvnN0-O-k/w377-h592-no/photo+5.JPG" alt="" width="191" height="299" />

Also, I graduated this week! I thought I wasn't going to, but I ended up doing well enough on that final I was worried about to pass. :P (Still didn't do well, but well enough.) Mindy came to commencement with Brittney too, it was fun to have them there.

<img class="aligncenter" src="https://lh3.googleusercontent.com/-wkw54R1TmJs/U1rN6LDYkOI/AAAAAAAAD6Q/PerWdRAk_yI/w1044-h587-no/DSC00017.JPG" alt="" width="375" height="211" />

I also had my last day of work with John this week. Leaving was actually kinda hard, it feels like I've been there forever.

<img class="aligncenter" src="https://lh4.googleusercontent.com/-KaRQfFI-rOE/U1m7lGcjmnI/AAAAAAAADww/gSTazScuKlk/w789-h592-no/IMG_20140424_115642.jpg" alt="" width="283" height="213" />

Tomorrow Brittney's Mom flies home, I start my new job at FamilySearch, and Brittney begins her full-fulltime job as a Mom. We're so excited to be moving forward with our lives. Favorite scripture of the week: <a href="https://www.lds.org/scriptures/bofm/alma/58.40?lang=eng" target="_blank">Alma 58:40</a>. We love you all!
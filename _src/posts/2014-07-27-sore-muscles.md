    Title: Sore Muscles
    Date: 2014-07-27T19:34:02
    Tags: Weekly Letters

Hey Family,

We were glad to hear that Mom, Dad, Joseph, Becky, and Jared all made it home safe after the trip. We enjoyed having you guys out here. :)

This week has been so exhausting. :) Tuesday night was super fun, we went to the rec center and played racquetball with everyone before they left. Mike and I played a super fun game, we were neck and neck the whole way and both of us had some really fun saves and shots. The next day, I was so sore.

Thursday was Pioneer day. We got up around 7 to run in the Temple to Temple 5K. We left our home around 7:15 with Mindy, Rachel, Heather, and Kristine, and got to the temple right as it was starting at 8. We met Michael and Travis there, and all started together. I was planning to just walk it with Brittney, but then I saw Kristine take off, and I thought "I can't let my little sister-in-law beat me!" So I decided to start running, and I didn't stop. :) Later on after we had all finished, we were comparing times and figuring out how fast we all ran it. The clock said 27:26 when I finished, so I told everyone that I hoped we had crossed the start line at 8:07, cause I would feel really good about running it in 20 minutes. Then Travis piped up and said we had started at 8:10! I was pretty happy with myself, new personal record. :) We also saw a couple of old friends from Brittney's singles ward, it was fun to catch up with them. :)

After the race, we headed home, cleaned up, and got ready for our mountain picnic. We had planned to go up in the mountains behind Squaw Peak, grill some hot dogs and pineapple, do some mountain biking, play some games, and just chill. Well, we got up there and set everything up, and then realized how hot it was, even in the shade with a breeze. Abby started getting cranky, and we saw smoke on a neighboring mountain and 3 fire helicopters circling it. We decided to leave. :) So, 45 minutes after arriving, we took everything back down to the cars and left. Kristine and I did get to bike down to the overlook from the campsite though, so that was fun. I also just coasted on my bike down to Provo Canyon after we left the lookout, and Mindy drove the truck down. I felt like Walter Mitty longboarding down the volcano. :) It was pretty fun. :)

We came back to our apartment and just relaxed. We watched some White Collar, ate dinner, normal stuff. We were so tired.

Friday I went back to work, pretty normal day. Still very tired, and extremely sore. :)

Saturday was a chore day. We balanced our budget, went grocery shopping, and did the laundry. We spent some time reading and playing with Abby, and then attempted to grill vegetable shishkebabs for dinner. :) We went to grill them at a park, but we weren't really prepared. We didn't have a brush to clean the grill, and I just started a fire and threw one on because we were hungry. It turned black in a hurry, without even being cooked. Yuck. For the others, we decided to seal them in tin foil and cook them that way, they were much more delicious. :) After that we watched White Collar again and then went to bed.

Church today was good. Abby was kinda cranky, and spit up on me several times. :P The meetings were good though. Everyone but Mike came over for dinner, we had homemade burritos. Delicious. :) We're still tired (but not pregnant!) ;) and sore from the week's activities, so we're hoping to get to bed early tonight. Love you all!
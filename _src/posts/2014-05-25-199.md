    Title: Days Go By
    Date: 2014-05-25T21:04:34
    Tags: Weekly Letters

Hey Family!

What happened in our life this week... Abby has been making more non-grunting sounds, that's been fun. Brittney has also been standing her up and letting her support her own weight, she's been doing really well! She also turned one month on Monday, so Brittney did a little photo shoot to celebrate. :) I'll share the pictures later. :)

Work is going well for me, I'm really enjoying being able to contribute more and getting more comfortable with everything. I had a fun experience this week. Steven and I were making a change to all of our load balancing servers this week. That's like doing surgery, if we do something wrong, FamilySearch.org could stop working. :) Well, Steven has only been there since January, so we're both just fledglings in this environment. As we were making some of the changes, we noticed that some of our internal websites stopped working...and we just about had heart attacks. :) Come to find out we were fine, another group was changing some network stuff, and it was them. Phew. :)

This weekend has been great. I got up early and went to the Temple yesterday for the first time since Abby was born. It was so nice to go back. I ran into Mike when I was walking out too! Always good to see family at the Temple. :) The rest of the day we ran errands, cleaned re-arranged our room, etc. Then we watched Charly last night. What a terrible movie. ;)

Church today was awesome. We're excited for the holiday tomorrow too. We're going to drive the Alpine Loop. :) Love you all!
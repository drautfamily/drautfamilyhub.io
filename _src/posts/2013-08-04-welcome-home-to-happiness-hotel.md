    Title: Welcome home to Happiness Hotel!
    Date: 2013-08-04T12:43:27
    Tags: Weekly Letters, Pictures

Hey Family,

Hope you're all doing well, what a crazy week we've had here. :) Robert showed up on Monday, Mike showed up on Wednesday, a car-full of Kristine's things showed up on Friday, and Robert left this morning. Phew. :) We love having Mike here, and we loved having Robert here too, but we didn't hardly see him.

Wednesday night we had dinner with another couple in our ward, the Wildings. Michael showed up just in time too. :) Dinner was delicious, and we loved just visiting with them. We got along really well.

Thursday night we went and saw the Croods again, but with Michael and Kristine so they could see it too. It was even better the second time. Can you tell I really like this movie? :) Afterwards we just came home, ate popcorn, and relaxed.

Friday we helped Kristine clean and move her stuff out of her apartment. She and Robert are going home for a couple weeks, so we're storing all their stuff <em>chez nous</em> until they come back and can move into their new apartments. It was so hot! So we went swimming afterwards for a bit. :) Saturday morning we played racquetball with Michael. Holy smokes. We played so sloppily, but had such a good game. I'm pretty sore today, it feels good.

Brittney did a little bit of work on her classroom this week, and tutored some more. She's enjoying it, and the parents want her to come back once a week during the school year too. She also got into an ESL class that she wanted to take through her school next year, she's really happy about that too. :)

I haven't had a ton of time to work on Greebles this week, but I have been corresponding a bit with Peter, the original creator. I ran my intentions by him regarding how I wanted to release the game when I finish, etc, and got all that figured out. I'm hoping to launch a kickstarter project to raise some money for a testing computer this week. Wish me luck!

Church today was great. We had a good testimony meeting and good lessons. We're going to go visit Grandma a little later this afternoon too. We love you all!

P.S.

<a href="http://images2.wikia.nocookie.net/__cb20130227134207/the-croods/images/thumb/c/c2/Belt_tongue.png/496px-Belt_tongue.png"><img class="alignnone" alt="" src="http://images2.wikia.nocookie.net/__cb20130227134207/the-croods/images/thumb/c/c2/Belt_tongue.png/496px-Belt_tongue.png" width="179" height="172" /></a>
    Title: All Aboard the Roller Coaster
    Date: 2015-05-31T10:49:10
    Tags: Weekly Letters

Happy Birthday Becky! Hope your party was fun! What a week this has been. We've really enjoyed having Dad out here and seeing the new house. We can't wait for you guys to move in. 

Memorial day was super relaxing. Abby had been pretty cranky, but since we spent the entire day outside, she was much better. :) We woke up and went for a walk to the duck pond first thing. It was a little overcast, but nice and cool. From the duck pond we went up to campus and just walked all over before coming home for Abby to take a nap.

<!-- more -->

After Abby's nap we set up the bike trailer and rode over to JDawg's for lunch. Abby enjoyed bobbing her head to the music they had playing. :) After eating, we rode our bikes up to campus again and rode around a bit. Abby is enjoying the bike trailer a little more now, but she still doesn't like to be in it for more than a few minutes at a time. After the bike ride we came home and Abby took _another_ nap. :) 

Tuesday was pretty routine. I went back to work and Brittney had a good day at home with Abby, she was starting to become happier again. Tuesday night I went to the Temple, and then we decided to spend Wednesday with Dad. (Best decision ever.)

Wednesday morning we drove down to Spanish Fork to see Mom and Dad's new home. As we drove down and waited for Dad, we kept talking about how beautiful the area was, and how good we felt down there. :) As we walked through the house with Dad, Brittney kept saying "I want a house. Maybe we should start looking for a house." 

After walking through the house, we went with Dad to the different schools to get enrollment paperwork, and just enjoyed seeing more of the area. Brittney started wigging out once she realized that Sierra Bonita was the best Elementary school in the area. :) All the while, Abby was phenomenally well-behaved. Zero complaints every time we put her back in the car seat. It was amazing. :)

We went to lunch and to see a model home with Dad, and just started tossing ideas around. After talking a bit, we started to think that maybe we should start looking for a home right now. Everything felt right, so Brittney called Jenny to set up an appointment for that afternoon. :)

We met with Jenny at 4, and started the process. The next evening after work we went back out to Mom and Dad's area, and decided to put money down on a lot! Holy Smokes! The past couple of days have been such a wild ride, with a lot of excitement, but also a lot of discouragement. All the while though, it still felt right to keep moving forward. We're so thrilled. We've been back down several times to see Dad's house again and go by our lot.

I think that's about it for our week.. :) Happy birthday again, Becky! Love you all!


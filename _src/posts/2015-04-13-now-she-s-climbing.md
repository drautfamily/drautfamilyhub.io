    Title: Now she's climbing!
    Date: 2015-04-13T16:06:31
    Tags: Weekly Letters

Hey Family! Hope you're all doing well. It was good to talk to Mom and Dad last night for a little bit, we're excited to see you guys again. :) 

Abby is walking everywhere now! She's still a little wobbly, but she definitely prefers walking to crawling. She's also been learning to climb up on boxes and stools and shelves, so we've had to adjust our assumptions about how high she can reach! :) She's also been trying to learn how to get down from places on her own. (Like our bed.) She used to just leap headfirst off, knowing we would catch her. She's smarter than that now. :) She knows she needs to turn around, but she's still figuring out how. She'll turn herself sideways, but then she stops, like she can't figure out which muscles to move next. :)

<!-- more -->

She's been getting better at signing things too, which is really neat to see! It's really nice when she knows the sign for something and can show us exactly what she wants. She's been trying to say more words too. She can say "Ow" very clearly when she is hurt, as well as "No" if we're doing something she doesn't like. :) She's been working on "Ball" too, but mostly just makes the "b" sound. :)

We've had a good couple of weeks. We enjoyed General Conference and Easter. Work has been going well for me too. They've doubled the size of our team because it is becoming more and more urgent that other teams move their applications to [AWS][1], and our team makes that possible for them. I've also been asked to participate on a "Move Council" to prepare requirements that we'd like to be considered if they build a new office building for FamilySearch/ICS in Lehi, which has been fun so far. Unfortunately a new building is at least 3 or 4 years out. Oh well. :)

Brittney has been working on getting her teaching license renewed. She finished that up last week, so she's set for another 3 years. :) She's also been looking at taking classes online towards a Master's degree, which she's excited about. :)

We found out this week that Spencer unfortunately didn't make it into the Air Force Academy, so he'll be joining the clan here at BYU in the Fall! (We're definitely going to need to find a bigger place for Sunday dinners!) We're excited to have him out here too. 

This weekend we cleaned our bikes and ordered a bike trailer. We're really excited to go for bike rides again. We haven't really gone since we were dating and lived near the river trail.

I think that's about it...we love you all!

[1]: http://aws.amazon.com/
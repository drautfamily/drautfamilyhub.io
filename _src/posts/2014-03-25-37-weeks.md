    Title: 37 Weeks 
    Date: 2014-03-25T19:05:22
    Tags: Weekly Letters

(3/16/14)

Dad got back today! I think he was at a wedding or something? I can never stay awake long enough to hear a whole conversation. It sounded like he had a good time though. He kept saying how nice it was to see everyone. He was raving about his 'rattlesnake pasta' too, whatever that is. Mom's glad he's back. I am too, it's nice to hear their voices again.

(3/17/14)

I'm so tired of being squished! Mom has been having contractions all day today! I have to kick so hard to get comfortable again after each one.

(3/18/14)

Phew. Tuesdays are long sometimes. Mom had a doctor's appointment this morning, and then she had her ESL class tonight. I'm pooped!

(3/19/14)

Mom is so funny sometimes. She keeps telling me I need to come the weekend of general conference. If I had any control over it, I would be here already! Dad just laughs when she tells me that. :)

(3/20/14)

<span style="line-height:1.5;">Mom went back to school today. I slept most of the day, it rocks me right to sleep when she walks around. It's getting too cramped in here to do anything else anyways.</span>

<span style="line-height:1.5;">(3/21/14)</span>

Mom and Dad went to a concert tonight, there were so many voices and sounds! I get squished when Mom sits down for too long though, sometimes I kick her in the ribs to get her to move.

<span style="line-height:1.5;"> </span>
    Title: Happy Birthday Mom!
    Date: 2014-02-23T21:05:16
    Tags: Weekly Letters

Hey Family, hope you're all doing well. Happy birthday Mom! Hope it was great. :)

We went to the doctor again this week, baby is still doing well. We're getting closer, 7 weeks left! We also went to a prenatal class at the hospital this weekend. We got lots of questions answered and lots of good information. The class was a beast though, we were there for 3 hours Friday night, and all day long Saturday.

Not much else happened this week. I spent most of my time trying to understand the <a href="http://en.wikipedia.org/wiki/Fourier_transform" target="_blank">Fourier Transform</a> for CS 450. :) Today has been good. Our home teachers came over, and then we went for a walk and over to Michael's for dinner. The weather has been so beautiful this weekend! Love you all!
    Title: Welcome to December
    Date: 2014-12-07T14:33:08
    Tags: Weekly Letters

Hey Family,

Hope you're all doing well. Brittney and Abby are at church, I've had a gnarly sore throat all weekend, so I'm staying home today.

<!-- more -->

Monday night Michael and Mindy came over to say hi and brought us some delicious brownies! We had fun with them for a while, seeing their new car, playing mario kart, and just visiting.

Tuesday was a pretty routine day. At work we had the Family History Department Christmas Devotional. Elder Falabella spoke and then they had a nice lunch for us. It was a good day.

Wednesday Brittney had activity days, so Abby and I hung out together while Brittney was gone. We built some Jenga towers and played with her Little People nativity before she went to bed.

Thursday. What happened on Thursday. I went to the Temple in SLC, that was good. It was a regular day at work. Friday I had an appointment with the eye doctor to check on how contacts are going. I wasn't feeling great, so I decided to work a little from home, and then take the other half of the day off. Brittney and Abby went to visit Grandma Judyth. Friday night we stayed home and watched a movie.

Saturday I woke up with a worse sore throat! (Yuck!) I watched Abby while Brittney went to the Temple. That afternoon was our ward Christmas party. We went by just for a few minutes to say hi and grab a bite to eat. It was fun to see everyone there though. That night we watched "Heaven Can Wait." We liked it, but it was slow, and kinda heart-wrenching. :)

Today is Sunday. We're still having anyone over who wants dinner, but we said come at your own risk. :) We're looking forward to the Christmas devotional this evening. We've been trying to do mini-devotionals each evening this month to help us keep things centered around the Savior. We've been reading Christmas stories and watching the Bible videos. Its been good. :) We love you all!
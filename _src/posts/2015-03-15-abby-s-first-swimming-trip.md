    Title: Abby's First Swimming Trip
    Date: 2015-03-15T10:43:32
    Tags: Weekly Letters, Pictures

Hey Family! Hope you're all doing well! We've had an interesting week. Abby was being especially cranky and clingy on Monday, so Brittney took her temperature and discovered she had a fever of 102.9! That was a little scary. I came home from work and drove them to the doctor. They assumed it was an ear infection, but her ears were clear, so they said it was probably just a bug combined with teething. Apparently teething can cause a fever? We did't know that. :) Anyways, we gave her some medecine and she got better over the next couple of days. 

<!-- more -->

Tuesday she was still not feeling well. Brittney held her most of the day. Tuesday night I watched her while Brittney went to the Temple. (#TempleTuesday)

![](https://lh6.googleusercontent.com/-ZYEprLoudsc/VQXAa7zL2PI/AAAAAAAAQe8/aiA99F07ECI/w1482-h1984-no/IMG_1858.JPG)

![](https://lh4.googleusercontent.com/-HmMMdyPabFs/VQXAZZWHEwI/AAAAAAAAQe0/Lzm79-vtTRI/w1482-h1984-no/IMG_1857.JPG)

We had iteration planning at work on Tuesday. It went well. Our team just keeps getting better and better. It's awesome to see everyone learning more and to feel like we're working better.

Wednesday was a bit crazy. As soon as I got home from work, Brittney drove Kristen, our upstairs neighbor, and her 2-yr old son Caleb to the Urgent Care. He had dropped a 5lb weight on his big toe, and it was bleeding somethin' fierce. I watched their 5-yr old, Kylie, and Abby while they were gone. They got back a couple of hours later. Turns out he had chipped the bone on his toe, he's going to have a nice wrap for a few weeks. Poor guy. :(

After that, Brittney left to go to Mutual with the YW, and Michael came over to hang out, meaning we played Mario Kart and ate ice cream. :) He's getting better at it. :D

Thursday was a pretty normal work day for me. Brittney and Abby visited Grandma and tutored, so they were all over the place. Thursday evening we just relaxed.

Friday Brittney walked to DI with Abby and found some new toys. She found  a big bag of foam building blocks, but a lot of them had bite marks in them. Cleaning them all was our date night activity. :) The weather here has been gorgeous all week, (High of 73 today!) so we went for a walk too, and watched _Mulan_. 

Saturday was pretty quiet. We went grocery shopping, and tidied up the house. We decided to take Abby to the rec center to go swimming too. :) We were only there for about 30 minutes, but she seemed to have fun. She seemed really overwhelmed when we just sat and watched the other kids, but she loved going around the lazy river. :)

![](https://lh6.googleusercontent.com/-GV_wQzCFmRQ/VQXAir-QbfI/AAAAAAAAQfM/4CsZ1cQXzdM/w1482-h1984-no/IMG_1873.JPG)

I also got Abby's new ball stuck in a tree. Thankfully I got it down with our frisbee. :)

![](https://lh6.googleusercontent.com/-6gFIOVQMJ8E/VQXAf1i3rtI/AAAAAAAAQfE/NQ6KJ6mj8HE/w1482-h1984-no/IMG_1872.JPG)

Saturday evening we just relaxed at home. I did a little work, Brittney read, Kristined came by for a few minutes. It was quiet. 

This morning we went for a walk to the park. Abby had a lot of fun there. She loved going down the slide. We also played whack-a-mole. I'd stick my fingers up through the playground platform, and she would try to grab them, it was cute. :) Love you all!

[Funny Video of Abby][1]

[And Another][2]

[1]: https://plus.google.com/+BenDraut/posts/8r2jiPvawd9
[2]: https://plus.google.com/+BenDraut/posts/2yiJmo3zB8C

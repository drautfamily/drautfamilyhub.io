    Title: Merry Christmas!
    Date: 2015-12-20T09:59:34
    Tags: Weekly Letters

December is going so fast! Here are some of the highlights from our week:

* Abby has been talking so much! She repeats everything we say. She also tells us the color of _everything_ she sees. We enjoy listening to her.
* We finished the crib we were building for her doll for Christmas. It's not as pretty as we hoped, but Abby enjoys playing with it. :)
* I finally got Haystack working to the point where we can use it to get pictures/videos off of our phones. I still have a bit more work to do to let us browse them on our computers, but it has been fun to see the progress.
* We enjoyed visitng with Travis & Sarah and their families for a few minutes at their reception last night. We're happy for them. 
* At work, Chris is on vacation for the next couple of weeks, so I get to lead our team on the next couple of stories we're working on. We'll see how it goes... :)
* Brittney is counting down the days until Baby Boy is born. She's tired of being pregnant. :)

![](https://lh3.googleusercontent.com/KTNMRimr63MYmaWsf9a1g7WEkzgocyqa9NMrNVKAZy1E7FzxF5YKgYC1MKmIW4XU5QsllfF1msJ41JlbBvtZkvO0tC8gxFLbTWU75h0HdOFV1H-zQWM88TIKcHjNcBUPa2vmp6SwLrDITxfrpWNArcGwe0RCi-bgVMc4wp9lW5i77LMbllk7KFTMzblaNl8CpVKcnULYTKuGYwxHzkcHKNBkT0TOqqS4cHB-fEHxL5ievPm4vcxBYikrNQeCnEn6YhVaCKFULSF0URhJlejgx61E4PXMAJQ2Jh-4N3NfT1Vw05Ls_i5pX4cc2efWvC7JC2V-2-O9rHVq-eOuS3KcAbHY3rRBBZtyf11ip_Wat6rVDeID7QeCQz449oDekjed2sxieS5-cOK56zIu-8Kxzq0M2J2xEDXngH_s1g4zOA-pWNNR_yv_xvq65vzBOtZoENe4ftgRLJAPljpxqPsr6UsHy_Odql5fFWxlRW_V0KWSlVDhY4SpyvVdff34EYZCINcxr1iMHjbA8rc6145K_xj5vsvIbUZZoBMZ5YgGWOX1E6Q87I17AoIy-aWOtVcXnDVjwA=w3832-h2156-no)

Love you all!

    Title: Family Pictures
    Date: 2015-04-26T19:09:35
    Tags: Weekly Letters

Hey Family! We're excited to see Mom and Dad this week! Congratulations to Tiffany and Trevor on the new job! Congratulations to Michael and Mindy on the raises! Congratulations to Mike on being homeless! :)

We've had a fun family-packed week. :) Abby's birthday party was Tuesday. Everyone came over and we celebrated with cake and ice cream. Abby got some clothes, a bubble machine, a ball, a bath toy, and a few other things I think...the bubbles have been a big hit! :)

<!-- more -->

Tuesday morning I got a phone call from our Stake Executive Secretary saying that our Stake President wanted to meet with us. *gulp* Wednesday night Michael came over because he wants me to start teaching him how to make a website. It was fun trying to teach him, and he seemed to enjoy learning about the possiblilities that it opened. :)

Thursday night Michael and Travis babysat Abby while Brittney and I met with President Rasmussen. Afterwards we had fun talking for a little bit. Friday night Michael and Mindy babysat so that we could run out and grab a few last things for Family pictures on Saturday.

Saturday morning we drove up to West Jordan to take our family pictures. They turned out ok... :) Abby's individual pictures turned out better. :) We went to Zupas for lunch and then drove home. That evening, our neighbors were going to go to the Payson Temple open house, but they couldn't go anymore so they gave us their tickets! We got down there around 5pm. It was gorgeous!

Church was good today. Kristine and Rachel came with us since they're in between wards right now. Then everyone but Mike came over for dinner. :) Love you all! 

We're still uploading pictures/videos from this week, we'll send them out tomorrow probably. :)

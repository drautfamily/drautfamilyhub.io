    Title: Happy 4th!
    Date: 2015-07-05T20:06:16
    Tags: Weekly Letters

Happy 4th of July! Tiffany, Trevor, and Grandma, we missed you. :) The week was pretty routine. Brittney was still weak on Monday from being sick all day Sunday, but she was well enough that I went to work. She just chilled at home with Abby. Thankfully, Abby was willing to chill inside most of the day with Mom. :)

<!-- more -->

Wednesday was pretty quiet. Brittney had Young Women's in the evening, and I just enjoyed the quiet time at home. :)

Thursday I was going to go to the Temple, but I forgot that Provo was closed. :( I came home and we decided to clean the house really well. That turned out to be a really good decision, because we spent all day Friday down in Spanish Fork. :)

Friday was great. We went for a walk early in the morning before the sun got too high. We walked by an elementary school just as two hot air balloons were landing! It was neat to see them land and fold up everything. They're smaller than I expected... :)

After that we drove down to Mom and Dad's and just spent most of the day there. We left for a few hours to go to the design center up in Lehi, where we started picking out materials and such for our house. 

Friday night we did fireworks, watched Air Force One, and just hung out. One of the fireworks did spontaneously combust. It was pretty wacky. We had nothing to do with it surprisingly. :)

Saturday we took it easy. We were pretty tired from the day before. We watched a little bit of the parade, went grocery shopping, at lunch with the O'Dells, went to the rec center for a bit, and chilled at home. It was nice. :)

Today was good, a little cooler than it has been. We had a good lesson in Elder's Quorum about the Sacrament. You should all read [this talk][1]. :) The Gospel is true, love you all!

[1]: https://www.lds.org/general-conference/2014/10/the-sacrament-a-renewal-for-the-soul?lang=eng



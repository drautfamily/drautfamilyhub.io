    Title: Almost a Year Old!
    Date: 2015-04-19T08:55:27
    Tags: Weekly Letters

Hey Family! Hope you're all doing well, we're happy to hear that the house was listed! We [found dad][1] in a YouTube video that the agency posted. :)

<!-- more -->

Abby's birthday is coming up on Tuesday! She'll be a year old. Sometimes we feel like we've had her forever, and other times we can't believe it's already been a year. :)

Grandma Judyth sent Abby a card and $5 for her birthday, so we went down to the bank yesterday to start a savings account for her. She had a lot of fun running around the lobby exploring. The best part was when she started walking towards the glass doors, and walked right into the wall. :) They have floor-to-ceiling glass windows next to the doors, and Abby walked right into one at full speed, I was dying. :)

We also got a bike trailer this week. We put it together yesterday and took Abby on a short bike ride. She was a little scared at first, but then she started enjoying it. (Until the sun starting getting in her eyes.)

The rest of the week was pretty routine I think. Brittney and Abby have had a good week at home. Abby has at least one tooth poking through now. I had a good week at work, not much to report there. :) Love you all!

[1]: https://youtu.be/jLCi2-Tzdhc?t=4m
    Title: Merry Christmas!
    Date: 2015-12-27T09:40:32
    Tags: Weekly Letters 

You get _two_ posts titled "Merry Christmas!" this year! You lucky dogs. :) It's been a great week. We've loved being able to spend so much time with family. 

The beginning of the week was a little bit stressful at work. Chris is on vacation, so I've been leading our team. It's an interesting perspective change, thinking about the road ahead so that others have work to do. It's a bit more pressure. :) We have a lot of unknowns with our current story, so I've been worrying about it a bit, but on Wednesday we had a couple of breakthroughs, so that was good.

<!-- more -->

Spencer and Kristine have been home in MI all week, but Heather didn't leave until Friday, so she and Brittney hung out a lot with Abby. They had fun together. We've been FaceTiming a lot with them too. Abby is hilarious. Sometimes she only wants to see Spencer or Grandpa Barlow, so if anyone else comes on the screen, she looks away and just says "Spencer. Spencer." 

On Christmas Eve, we went to see Grandma Judyth in the morning. I haven't seen her in a while, so that was nice. Around 11 we headed up to Mom and Dad's and spent the rest of the day there. We had fun just being together there. Michael, Joseph, Jared, and I had a pretty good snowball fight. I took way too many shots to the head though. :) After dinner we did _Christmas with the Wright Family_ and then we headed home. Heather gave us _Inside Out_ for Christmas, so we watched that after putting Abby to sleep. Or we watched the first half of it. Brittney was super tired, and our DVD player started skipping too much to enjoy the movie. So we called it a night. (It was only 8:15 or so)

Christmas morning we both woke up at about 5am, ready to go. :) Abby of course, was still sound asleep. We made crepes for breakfast while we finished _Inside Out_ (What an emotional roller coaster!), ate breakfast, and then we started playing [_Splatoon_][1] while we waited for Abby to wake up. It was a White Christmas too! We got 4-6 inches in Provo. It was beautiful.

Abby woke up around 7:45, and we started opening presents. Abby just wanted to play with each gift that she opened, she never wanted to move on to the next present, so it took a while. She got some nail polish, a puzzle, some megablocks, a stroller, and a few other things.

Around 10 or 11 again we headed down to Mom and Dad's. We had fun spending time there again. We played some games, set up the Wii U, opened some more presents, it was a good time. :)

Saturday we went down again, after doing our laundry and grocery shopping for the next couple weeks. Our apartment is just so small. We don't want to hang out here all day. :) They have been working on our house a lot too! It looks like they're getting close to finishing the framing. We're hoping all the snow and ice will melt or dry or get off the house. :) 

I don't have any meetings this morning, so we're just enjoying the peaceful morning. Tomorrow it's back to work for three days before the next 4-day weekend. :) Love you all!


[1]: https://www.youtube.com/watch?v=Grs_V9gT-ls
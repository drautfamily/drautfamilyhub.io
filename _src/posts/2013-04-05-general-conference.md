    Title: General Conference!
    Date: 2013-04-05T13:47:05
    Tags: Weekly Letters, Pictures

Howdy family!

What a great week this has been! Brittney's family spent spring break out here, so we've had fun hanging out with them. We all had FHE together on Monday night and watched Finding Faith in Christ for Easter. :) We also had dinner at Cafe Rio so that we could say hi to Robert again. :)

Brittney and I went dancing Tuesday night and had a great time. We tried salsa for a bit, and then decided to go back to country. :)

We picked out the pictures for our announcements as well. Angie is going to order those next week, and then we get label and address and send them all!

Huzzah. ;)

One of Brittney's mission reunions was Thursday night in Provo, so we headed over to that after Mindy opened her mission call. She was worried that she wouldn't know many people there, but we ended up staying for quite a while! It was fun visiting with everyone.

Friday was pretty busy. I drove Brittney's car up to her school around 4 to help out in her classroom, and then we headed over to her other mission reunion. :) We stayed there for about an hour before heading over to mine. It was good to see everyone again, especially President Staheli. He never lets us get together without hearing his testimony. :) He encouraged us a lot, and counseled us to stay with the brethren in supporting the family. It was good. He said "Whatever happens, I choose Jesus, and I choose Joseph." :)

Conference weekend has been great, we've been making breakfast and watching at Brittney's house. Kristine has been coming over too, it's been a good weekend. Lots of walks in the park. :)

This is the last week of school! I'm really excited. :) I've been working a lot on my Calvin and Hobbes game, its been coming along really well. I'll post a download link once I finish it.   In the meantime Tiffany can vouch for how cool the flamethrower looks. And here's a screenshot. :) And a picture from Mindy's mission call opening. :)

<a href="http://benandbrittney.files.wordpress.com/2013/04/screenshot.png"><img class="alignnone  wp-image-10" alt="Screenshot" src="http://benandbrittney.files.wordpress.com/2013/04/screenshot.png?w=300" width="180" height="113" /></a>              <a href="https://lh6.googleusercontent.com/-8HX9jrBNIHc/UV5HJqafHEI/AAAAAAAAAwY/KNM05PKCDx4/s679/IMG_20130404_181230.jpg"><img class="alignnone" alt="" src="https://lh6.googleusercontent.com/-8HX9jrBNIHc/UV5HJqafHEI/AAAAAAAAAwY/KNM05PKCDx4/s679/IMG_20130404_181230.jpg" width="146" height="110" /></a>

We love you all!
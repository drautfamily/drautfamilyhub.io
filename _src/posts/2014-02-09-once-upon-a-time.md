    Title: Once upon a time...
    Date: 2014-02-09T16:24:16
    Tags: Weekly Letters

there was a magical place where it never rained. The end. :) But that place wasn't Provo, we've been getting a bunch of rain lately.

We've had a pretty routine week out here, not a whole lot has happened. This weekend we did go start a baby registry at target. They gave us a $20 gift card for doing it, which we used towards some curtains for the nursery. They look nice.

Brittney also went to the fabric store. She finished the baby quilt she had been working on, so now she's going to start a car seat cover. :) Robert also came over yesterday for lunch and hung out with us for a little while. We hadn't seen him in a long time. We went out to dinner at Mimi's cafe with a gift card we had, and then we just came home. Brittney's back has been hurting more and more recently, we're really looking forward to April! I finished some homework and then we watched Daddy Daycare.

Church was good today. A lot of people talked about trials in their testimonies. One sister mentioned how much happier she had been as she tried to turn to the Lord as soon as something came up, rather than waiting until it was bad enough that she felt like she needed help. It was good. Heavenly Father wants us to turn to Him in everything. Love you all!
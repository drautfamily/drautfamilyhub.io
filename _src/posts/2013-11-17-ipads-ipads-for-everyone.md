    Title: iPads! iPads for everyone!
    Date: 2013-11-17T15:00:57
    Tags: Weekly Letters

Hey Family! Hope you're all doing well, we love hearing how everyone is doing.

So big news for this week: Brittney wrote a grant proposal back at the beginning of the school year to get a classroom set of iPads, and she found out this week that she got it! Now she has her own iPad plus one for each child in her class. The kids love playing different educational games on them, as does Brittney. :)

Brittney has another doctor's appointment this week on Thursday that we're looking forward too. The plan is to schedule the ultrasound for the week after, so we should know soon if it's a boy or a girl! Brittney says the baby has been moving around a lot lately too. :)

School for me has been crazy this week, and it looks like it will be for the rest of the semester...but it's still fun. :) I'm learning an incredible amount in my programming languages class, it's awesome. I'm having a mini-crisis trying to deal with the fact that I really don't understand how to use programming languages as well as I thought I did. :)

Thursday night we went to French Idol, the BYU French Club's version of American Idol. It was entertaining, but we both thought it was better last year. :) One of our favorites was a couple of guys that sang LDS parodies of popular French songs. :)

This weekend was pretty low-key. I had a lot of homework to do, and Brittney spent some time with Kristine and planned for school next week. We decided to make a pot roast for dinner today. (Mike, you should really come over!) It cooked all night so we woke up with a mouth-watering aroma in our noses. :) Church was great today. I especially liked Priesthood. Here's my favorite quote from the lesson:

“When you find yourselves a little gloomy, look around you and find somebody that is in a worse plight than yourself; go to him and find out what the trouble is, then try to remove it with the wisdom which the Lord bestows upon you; and the first thing you know, your gloom is gone, you feel light, the Spirit of the Lord is upon you, and everything seems illuminated.”

Have a great week, we love you!
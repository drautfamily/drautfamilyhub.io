    Title: We Thought We Were Through This!
    Date: 2015-05-10T10:13:55
    Tags: Weekly Letters

Hey Family! Hope you're all doing well. We've had a good week, though Abby has been teething a lot. She's been waking up every 1-2 hours this week at night, screaming and pulling at her teeth. :( It's been a little rough. Thankfully, she slept really well last night, so we hope we're out of the woods for the time being. :)

<!-- more -->

I'm trying to remember what we did this week. It has been raining most of the week, so that along with Abby's sleepless nights has made us all a bit morose. :) Monday was FHE, Tuesday Brittney went to the Temple, Wednesday Michael came over, nothing happened on Thursday, and Friday was date night.

For date night we invited Chris and Mercedes Anderson over for dinner. (I worked with Chris at BYU.) They have a 3-week old girl now, and are leaving for an internship in Arkansas in a few weeks, so we wanted to catch up. It was fun to visit with them a bit.

Saturday it was still nasty outside, so we spent most of the day de-junking. We took 4 garbage bags of clothes to DI, along with a few boxes of books. We also realized Brittney had a bunch of old textbooks that she didn't need anymore, and we could sell them back to Amazon for some money, so that was awesome. Our bedroom is a bit less crowded now too, which we're really enjoying. :)

This morning for Mother's Day Brittney wanted Crepes for breakfast, so we made those. She also got a new Kindle, since her old one broke. :(

I think that's about it for this week, not a whole lot going on. Love you all!

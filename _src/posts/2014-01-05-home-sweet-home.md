    Title: Home Sweet Home
    Date: 2014-01-05T11:05:10
    Tags: Weekly Letters

Howdy Family,

What a week we've had. We loved being home for Christmas and seeing everyone, but it's good to be home too. :) We flew out Tuesday evening from Detroit and landed in SLC around 9:30. We had a good flight, plenty of TV and reading. :) (I finally read Tongue of Fire, Mom!) We were going to take the train, but we missed the last one that would have gotten us home before midnight, so we decided to just take the shuttle. We got home around 10:30, put things away, and went to sleep. Then the fireworks started at midnight and woke us up, Happy New Year! :)

The rest of the week we just did things around the house. We cleaned out the baby's room, bought a crib, unpacked, did laundry, etc. We also went to Target to get a DVD player and some bedding for the crib, and found a stroller too! It was on sale for about $60 cheaper than normal, and Brittney liked it more than any others she had seen, so we nabbed it. :) We set up the crib and Brittney has been making her own crib skirt for it too, it looks good so far. :)

Friday night we went and saw Free Birds at the dollar theatre. It was pretty good, we enjoyed it. Saturday night we went up to Salt Lake to get Michael and Travis from the airport. Before that, we stopped and had dinner with Ben Avery at Bruges in SLC. It's a delicious waffle joint. :) It was fun to visit with him again. He started working full-time at a design company up in Washington this year.

We have church at 11am this year! So we're just having a calm morning getting ready for church. Later on we're going to get Kristine at the airport and then have everyone over for dinner. Then school starts up again tomorrow! :) We love you all! I'll put up some pictures of the baby stuff a little later.
    Title: First Sunday in our new ward!
    Date: 2013-06-23T15:00:12
    Tags: Weekly Letters

Hey Family! 

It has been a good week, we had lots of fun at Mackinac. Plenty of bike riding, fudge, reading, and fancy dining in the Grand Hotel. :) 

We got back late Wednesday night and went straight to bed. We spent most of Thursday running errands and writing thank you cards. It was nice just resting at home. We also made a delicious coconut chicken curry for dinner. So good. :) 

Friday it was back to work for me. Brittney stayed home and took care of some other things, she also had one of her friends over for lunch. Friday night we volunteered at the MTC again, which was nice, it had been a while. :) After that we had dinner and played a round of chess. Brittney beat me, so it instantly became one of her new favorite games. ;) 

Saturday was a fun day. We made waffles and fruit smoothies for breakfast with our new waffle maker and magic bullet. :) then I helped an old friend move some furniture, and Brittney and I rode our bikes up Provo canyon to Vivian park. It was a beautiful day. :) We also went and saw monsters university, which has was  pretty good. I think our expectations were a bit high. :) 

Today was our first Sunday in our new ward, and we love it so far. We know a surprising number of people already, and everyone else is pretty outgoing. We're excited to be here. :) We love you all!
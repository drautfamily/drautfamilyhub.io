    Title: Raspberry Picking!
    Date: 2014-09-21T16:13:53
    Tags: Weekly Letters, Pictures

Hey Family,

We've had a great weekend out here, and I think the week was good too, we'll see what we can remember. :)

Last Sunday we went for a walk around the Riverwoods to see the art from "Chalk the Block", there were some pretty good ones:

<a href="https://lh4.googleusercontent.com/-OohCKcaQOp0/VBZHFB1b-lI/AAAAAAAAE8g/65pmYsUE2_M/w415-h553-no/IMG_20140914_184134.jpg"><img class="aligncenter" src="https://lh4.googleusercontent.com/-OohCKcaQOp0/VBZHFB1b-lI/AAAAAAAAE8g/65pmYsUE2_M/w415-h553-no/IMG_20140914_184134.jpg" alt="" width="215" height="286" /></a>

&nbsp;

Brittney took Abby to the park and put her on the swing for the first time too:

<a href="https://lh5.googleusercontent.com/-KV47rplI7HE/VB9H8EG5uqI/AAAAAAAAFC0/YeN9Fj60M4s/w413-h553-no/IMG_1039.JPG"><img class="aligncenter" src="https://lh5.googleusercontent.com/-KV47rplI7HE/VB9H8EG5uqI/AAAAAAAAFC0/YeN9Fj60M4s/w413-h553-no/IMG_1039.JPG" alt="" width="212" height="284" /></a>

&nbsp;

I also finally got to move cubicles at work! Now I'm in my team's area, and I get a window seat to boot! :) Here's my view when I look up:

<a href="https://lh3.googleusercontent.com/-gq5fiD_XQrA/VB9IQvmKyCI/AAAAAAAAFFM/dbHef5Joze4/w737-h553-no/IMG_1060.JPG"><img class="aligncenter" src="https://lh3.googleusercontent.com/-gq5fiD_XQrA/VB9IQvmKyCI/AAAAAAAAFFM/dbHef5Joze4/w737-h553-no/IMG_1060.JPG" alt="" width="332" height="249" /></a>

&nbsp;

The Temple is just offscreen to the right, I can see it if I lean waaay over. :) Here are some bonus pictures I've taken recently:

<a href="https://lh3.googleusercontent.com/-hzo35DWuppw/VBwyISBbalI/AAAAAAAAE_A/hDK7SQRU5-0/w371-h495-no/IMG_20140919_071133.jpg"><img class="aligncenter" src="https://lh3.googleusercontent.com/-hzo35DWuppw/VBwyISBbalI/AAAAAAAAE_A/hDK7SQRU5-0/w371-h495-no/IMG_20140919_071133.jpg" alt="" width="163" height="217" /></a>

&nbsp;

<a href="https://lh3.googleusercontent.com/-G4RtJf5upE8/VBGQ8yy_w3I/AAAAAAAAE7o/jJUUX8QEduM/w920-h311-no/PANO_20140911_060341.jpg"><img class="aligncenter" src="https://lh3.googleusercontent.com/-G4RtJf5upE8/VBGQ8yy_w3I/AAAAAAAAE7o/jJUUX8QEduM/w920-h311-no/PANO_20140911_060341.jpg" alt="" width="541" height="183" /></a>

Thursday morning I went to the Salt Lake temple before starting work. It was my first time there, I enjoyed it a lot. Since I started a little later than normal on Thursday, I stayed later on Friday. Brittney went to Mindy's bridal shower, and then Heather and Kristine came over to have a little girls night. I ended up getting home around 7:30, I didn't mind missing the girls' festivities. :)

Saturday morning we got up semi-early, (around 7) and drove down to Payson to pick raspberries. It was just one family's farm, in the middle of their neighborhood. They had a table with a scale and a money bucket set out, totally self-serve. We picked about 4 pounds and called it good. :)

<a href="https://lh4.googleusercontent.com/-zIYgmJJpNmg/VB4ebOhuLXI/AAAAAAAAE_0/0FyaNgEGUzo/w737-h553-no/IMG_20140920_083518.jpg"><img class="aligncenter" src="https://lh4.googleusercontent.com/-zIYgmJJpNmg/VB4ebOhuLXI/AAAAAAAAE_0/0FyaNgEGUzo/w737-h553-no/IMG_20140920_083518.jpg" alt="" width="217" height="163" /></a><a href="https://lh5.googleusercontent.com/bVRXJ7zHX5nsl9H-8_axVVSqHspXVSaktUnDofz8fYo=w413-h553-no"><img class="aligncenter" src="https://lh5.googleusercontent.com/bVRXJ7zHX5nsl9H-8_axVVSqHspXVSaktUnDofz8fYo=w413-h553-no" alt="" width="176" height="236" /></a> We stopped by Grandma Judyth's on our way home to give her some raspberries and just visit for a few minutes. It was good to see her again. We came home, took care of the raspberries, and had lunch before going grocery shopping. Saturday evening we made rice and shishkebabs for dinner, and walked up to campus to have a little picnic. :) That was fun too, Abby enjoys playing with grass. :)

<a href="https://lh3.googleusercontent.com/-wYQiAyPHZ6E/VB4ec69CKxI/AAAAAAAAFAA/PeBUVOoL3xc/w737-h553-no/IMG_20140920_174322.jpg"><img class="aligncenter" src="https://lh3.googleusercontent.com/-wYQiAyPHZ6E/VB4ec69CKxI/AAAAAAAAFAA/PeBUVOoL3xc/w737-h553-no/IMG_20140920_174322.jpg" alt="" width="279" height="209" /></a>

After that we came home and watched a movie. Or tried too, rather, but Abby kept waking up last night... :) I've also been working on Simap a lot during my commute this week, I'm almost done figuring out how we want everything to look. You can see it <a href="http://drautb.github.io/simap" target="_blank">here</a>. It doesn't actually save anything yet, but you can get an idea for how it will work once it's finished. :)

We love you all!

&nbsp;
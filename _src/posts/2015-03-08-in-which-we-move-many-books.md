    Title: In Which We Move Many Books
    Date: 2015-03-08T05:05:04
    Tags: Weekly Letters

Hey Family! Hope you're all doing well. It sounds like you're having lots of fun getting ready to move. :)

We've had a good week out here, pretty calm. Brittney and Abby have been having fun at home. Abby is learning to communicate more, which is exciting to see. She'll point at things that she wants, and make gestures when she's hungry or thirsty. She tries to say "crac-ker" a lot. :) She's super ticklish too, she loves it when we make drill sounds and tickle her neck. :) She still hasn't found reason to walk yet though. 

<!-- more -->

Brittney started reading _The Count of Monte Cristo_ this week, unabridged. She was captivated for the first 20% of the book, then she said it slows down a lot. She's enjoying it though. She's also been watching a lot of _Baby Signing Time_ with Abby. :)

Work has been going really well for me, it has been a great week. We've just been working really well all together and getting a lot done. It feels good to be productive. :) At home I've been doing a lot of computer stuff while Brittney has been reading. Our upstairs neighbors have a desktop computer that's about 4 years old. They haven't been able to get it to run for the last year though, so they asked me if I could get their pictures, etc. off of it for them. After doing that, I tried to get it working with Windows again for them, but it wouldn't work, so they said we could keep it. I put Linux on it and have just been tinkering all week. :) It's still a good computer, so we may keep it for a while.

Thursday after work I drove up to Ogden to get Dad's books. Holy heavy boxes, Batman! I got up to Ogden around 5pm, and then got back to Provo around 7:30pm. They've started doing construction at the point of the mountain, so rush-hour traffic was extra nasty.

This weekend was fun. Friday night we were just relaxing at home, and Michael came over so I could look at his computer. We had fun taking it apart and shaking all the rice out of it. :) It looked dry and fine, but his battery was dead, so we couldn't test it. After that, we started watching _The Book of Life_. We've been telling Michael how good it is, but he still hadn't seen it. We got about halfway through before it got too late to finish. After Michael got home, he texted me "Hey, the computer turns on, but the keyboard isn't working..." I had forgotten to plug it back in when we put it back together. :P

Saturday afternoon we drove down to Grandma's to drop off Dad's books and visit for a bit. She was happy to see us and take a break. She spent most of the day with Kim. Grandma said she's doing much worse. :( Saturday evening Heather and Kristine came over for a bit, and then Michael and Travis came over! I fixed Michael's computer again, and then we finished watching _The Book of Life_. After the movie, Heather and Brittney went to Walmart, and the boys played Mario Kart. Fun stuff. :) By now, it's about 8:45pm, and we all know it's daylight savings time, so really it's more like 9:45pm. Mike and Travis left, and Brittney and Heather got back around 9:15pm. Kristine came back over too after her date, and they decided to watch _Sense and Sensibility_. (Now I get all the jokes you guys used to make, F Major!) Eventually everyone went home around 12:30am, and we crashed. We were dead this morning when Abby woke up. :)

Love you all!

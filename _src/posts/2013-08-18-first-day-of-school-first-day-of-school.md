    Title: First day of school! First day of school!
    Date: 2013-08-18T15:52:23
    Tags: Weekly Letters, Pictures

Ok, not for me yet, but for Brittney! Her kids come back to school tomorrow! She's a little nervous, but happy to start too. She has a nice big class this year, 28 so far, and she thinks she'll get a few more. She's going to be great. :)

This week has flown by, but it has been awesome. We had FHE on Monday, and just relaxed at home. Tuesday night we went over to our friends, the Wildings, for dinner. Whitney is starting her teaching internship this year, so she and Brittney get along well, and so do Luke and I. We have fun with them.

Wednesday was probably the most exciting day of the week. We had plans to have the Pendleburys over for dinner at 6. I got home around 4, and decided I would get everything ready so that Brittney could just come home and relax until they showed up. (This was Brittney's first real work day back at school) So I start cutting stuff up for this soup we were going to make. I decided to use our nifty mandolin slicer to save time. It looks something like this:

<img class="alignnone" alt="" src="http://www.dehydratorbook.com/image-files/pinzon-mandoline-slicer.jpg" width="144" height="136" />

So I start slicing carrots with it, being extra careful because Michael cut his finger the last time we used it. It's going just fine, and then Brittney calls me. We talk, I hang up, and then I go back to cutting. But I forgot to focus. Ooops. Next thing I know, the celery stalk is getting really close to my finger, and all of a sudden I cut my thumb. Aww shoot.

Well, long story short, I couldn't stop the bleeding on my own at home, so Brittney took me to the instacare place by the hospital while I kept pressure on it. They ended up just constricting my thumb to stop the bloodflow long enough to clean it and bandage it. They said it would probably bleed through (which it did), but that was expected. Ok. Whatever.  It's much better now, it doesn't slowly leak blood all day long anymore, so that's nice. :) If you want to see a picture of it after it was cleaned, <a href="https://lh6.googleusercontent.com/-fCuu2I0vfJI/UgweZdqDpgI/AAAAAAAACZw/30e1RpjKOAg/w425-h566-no/IMG_20130814_175917.jpg" target="_blank">click here.</a> No blood, but maybe a bit shocking...

We pushed dinner back to 6:30 with the Pendlebury's and had a wonderful time. :)

Friday night Ben was in town for graduation, so we went out to dinner with him and just visited a bit. He's back in Seattle now working full-time for a design company called Kaleidoscope. Afterwards Brittney and I played Chess and Checkers at home. :)

Saturday went by fast. We played racquetball with Mike in the morning. We played three games with all three of us, one to 11, one to 15, and the last to 21. Brittney was winning for a good part of the last game too! I saw Mike's letter, I'll let you decide the outcome based on that. :P Ok, no I won't. I won all three games, even with a busted thumb. :D

That afternoon Brittney and I went up to Kaysville for a mini-reunion at her Aunt and Uncle's home. It was fun to visit with everyone there too. :) Today has been really good. Church was good, we went home teaching and that was good, Sunday is just a good day. Be grateful, and you'll be happy. We love you!
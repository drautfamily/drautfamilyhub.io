    Title: Travel Time! 
    Date: 2013-12-23T10:51:31
    Tags: Weekly Letters

Hey family!

We're glad you got power back already after the storm! Thankfully it wasn't as bad here in Troy. :)

We had a pretty fun week. I had <span style="line-height:1.5;">4 scheduled finals, and they were really spaced out, so it was a low stress week. :) The semester is over! Brittney had a fun week too, they saw the nutcracker and had a couple of parties. :)</span>

Friday night Brittney, Kristine, Michael and I took the train up to SLC. We had dinner at City Creek and then saw the lights at temple square. It was super cold. :) We took the TRAX to the airport around 9. We tried to get Kristine and Michael and Travis on the same flight as us, but there was no room. :(

Our flight took off around 1:30 am and arrived around 6:30 in Detroit. We slept some on the plane, but we were still so tired. Brittney 's dad picked us up and drove us home. We crashed and slept until about 2. :) We left for Brittney's family's Christmas party around 3:30 and got home around 9. We had a good time, but we were all pretty tired.

Yesterday we went to church and then just spent the afternoon talking. The sisters came over for dinner, and we just talked more afterwards. :) Today we're running lots of errands and then Brittney's grandparents are coming over for dinner tonight. Love you all!
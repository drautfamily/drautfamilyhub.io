    Title: Is it over?
    Date: 2015-09-13T10:53:05
    Tags: Weekly Letters

Hey Family! This week has felt like a tornado. Thank goodness for weekends. :) Monday, for Labor Day, we did some housework and then spent the afternoon at Mom and Dad's. We had a bunch of fun playing soccer, eating pizza, and just hanging out. :) 

Tuesday it was back to real life. At work, we had our sprint planning meeting. I was rotating off of being on-call, and it has been a little hard to switch back into the normal groove. It's been getting better though. Tuesday night I went to the Temple, and Brittney stayed home with Abby. I also had my monthly interview with our Bishop that night.

<!-- more -->

Wednesday was probably the busiest day of the week. I got home from work around 5:30, we at dinner really quickly, and then Brittney left for a meeting and YW at 6. She came home around 7:45 so that I could go to a Stake training meeting at 8:00pm. That went until about 9:30, and I came home and crashed. :)

Friday afternoon I came home around 1:30pm. I worked on processing all the paperwork we've gotten in the mail over the past couple of months and making sure that our budget was up to date. I ended up spending a lot of time learning more about our retirement accounts. Friday night for date night we started sanding/stripping our table that Brittney wants to refinish. That was fun.

Saturday was non-stop. We got up around 7 with Abby, and had breakfast. Michael came over around 9 and I took him to work so that I could use the truck. A couple of us in the ward helped a member move some furniture, we had the missionaries over for dinner, we stocked up at the case lot sale, did some more work on the table, it was nuts. :) 

Regional conference today was good. It was nice not having meetings this morning. :) 

We found out at work this week that they've approved a flexible work hours pilot program for the Family History Department. It goes until Nov. 30th, at which point they'll do an evaluation. Our hours have already been pretty flexible, but we were expected to work 8 hours each day. Now, if we want, we can work 4 10-hour days, and then take Friday off for example. Brittney and I are trying doing 4 9-hour days, and then taking Friday afternoon off. I wouldn't see Abby at _all_ during the week if I did 4-10s. 

Love you all!




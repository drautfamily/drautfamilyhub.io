    Title: Happy Thanksgiving!
    Date: 2014-11-30T12:48:49
    Tags: Weekly Letters

Hope you've all had a great week! Ours has been fun. :) Monday morning I had an appointment with the eye doctor to get fitted for contacts, so I stayed home to work the rest of the day. (I'm enjoying contacts!) That afternoon we went to visit Grandma. Brittney had written down the 24th as her birthday instead of the 26th, so we brought her flowers and wished her happy birthday and everything, and then she said "I think there has been a misunderstanding..." :) We had a good laugh about it. :) It was fun to visit for a bit.

<!-- more -->

Tuesday was a pretty normal day, I went to work, Brittney stayed home with Abby. They did a lot of cooking to get ready for Thanksgiving. Wednesday was similar.

We've been thinking about getting a Wii lately. Brittney's family has one that they all love to play on holidays, and she misses that sometimes. We started looking around, Nintendo sells a stripped-down Wii mini for $99, so that got us interested. It turns out that we overpaid a few of Abby's hospital bills too, so we found out that we'd be getting a refund check. Long story short, we decided to just go for the Wii U. (It came with Mario Kart 8 too, which was a big plus. :) ) I picked one up from the Walmart in South Jordan on my way home from work on Wednesday. We wrapped it up that night and put it with the other gifts that we were going to put with Santa's presents on Christmas morning.

Wednesday evening we started talking more about how we were going to re-arrange for Christmas. We had been having a really hard time coming up with a good arrangement to include the Christmas Tree in our living room, so we decided just to try some things right then. We ended up pulling out most of our decorations and getting everything set up on Wednesday night. :) We managed to get the tree in, and it doesn't feel crowded. Yay!

Thursday was Thanksgiving! We relaxed in the morning and warmed up some food before going up to Dave and Kristen's around 11:30. We watched the dog show and football with them before eating at around 1pm. It was the three of us, plus them and their two children. We had fun together. :) After dinner we played on their Wii for a little while, had pie and ice cream, and then headed home to take a nap. :)

Friday was our super duper lazy day. We just bummed around home during the morning, we played Rummikub and Yahtzee, and watched a movie. We decided to get out in the afternoon and see what Black Friday pickings we could find after the rush. We spent about an hour at Kohls, and then Abby was too tired to go anywhere else, so we came home. Brittney wanted me to get some new clothes. :) We found some new pants, a couple of shirts, and a toy for Abby. Friday night we were so bored, we decided to let Santa come early, and we opened the Wii. :) Two hours and one long system update later, we finally played a few rounds of Mario Kart. It was a lot of fun. Brittney creamed me. :( But that's ok, she's always saying that we need to get some games that she can beat me at. :)

Saturday was chore day. We went grocery shopping and cleaned the house. We played Mario Kart again too. (I won this time! :) ) Around 4pm we headed up to Bountiful for the wedding reception of one of Brittney's cousins. We had fun visiting with some of her Aunts and Uncles, they're all great to be around. On our way home we stopped in SLC to see the lights on Temple Square. It was way past Abby's bedtime at this point, so we didn't spend long there, but it was fun. We got home around 8:15, put Abby to bed, and then watched a movie.

Today we're eating dinner with another couple in the ward, and then going to pick up Heather and Kristine from the airport. Should be fun. :) Love you all!
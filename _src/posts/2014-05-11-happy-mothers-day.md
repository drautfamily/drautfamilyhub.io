    Title: Happy Mother's Day!
    Date: 2014-05-11T14:45:53
    Tags: Weekly Letters

Hey Family,

Happy Mother's day Grandma, Mom, and Brittney. :)

First things first, we bought our plane tickets! Brittney is coming out on July 1st, and I'm coming on the third. We're going to bless Abby in Brittney's ward on the 6th, and I'm leaving that night. Brittney is coming back on the 8th. :) Can't wait to see y'all! :)

This week has flown by. Work is going great for me, each day I pick up another piece of the puzzle that we're building and things start to make more sense. :)

Brittney is enjoying staying with Abby, she's started laughing in her sleep now. :) Most nights she wakes up only a few times and goes back to sleep pretty easily, for which we're very grateful. :) Brittney, being the angel that she is, also tries to let me sleep through the night when I have to get up early to work. :)

We went and visited Grandma Judyth yesterday with Mike and Mindy. She was happy to see Abby again. Her neighbors also gave us a few of their old baby things, which we were grateful for as well. Life is good. Everyone is healthy and happy, work is going well for me, we feel very blessed. Love you all!
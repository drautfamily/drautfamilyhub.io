    Title: Life is Changing
    Date: 2014-05-04T15:33:27
    Tags: Weekly Letters, Pictures

Hey Family, 

Hope you're all doing well. :) Abby is doing great, she's as cute as ever. The three of us are relaxing on our porch as I write this. :) 


<a href="https://benandbrittney.files.wordpress.com/2014/05/wpid-wp-1399238696590.jpg"><img title="wp-1399238696590.jpg" class="alignnone size-full" alt="image" src="https://benandbrittney.files.wordpress.com/2014/05/wpid-wp-1399238696590.jpg" /></a>



She's been sleeping pretty well most nights, which we're grateful for. :) She doesn't make many sounds yet, but she does like to just look around at everything. :) 

Brittney is doing well, she's getting better and better at taking care of Abby and knowing what she's needs. :) She does miss getting out though, so we've been going on lots of walks. :) It warmed up this weekend, we've been in the eighties! The Petersens put the chairs back out on the porch, now we just need them to install our A/C unit again. :) 

This was also my first week of work at FamilySearch. I decided to work up in Salt Lake so that I could get to know the rest of the team better, so I've been taking the train up every day. It's been really good so far. The team seems excited to have me back, and I've enjoyed being there again. :) We love you all!
    Title: Happy Pioneer Day!
    Date: 2015-07-26T19:10:43
    Tags: Weekly Letters

Happy Pioneer day! We've had a good week. I loved the short work week. :)
Monday-Thursday were pretty routine, I don't remember a whole lot from them. We'll start with Friday. :) We woke up at 6 and had breakfast and got ready to go. Around 7 Michael, Kristine, and the rest of the family showed up, and we started walking up to the Temple. We got up there around 7:40, about 20 minutes before the race started. I spent those 20 minutes waiting in line to use an outhouse. :) 

<!-- more -->

By 9 we had all finished the race. We hung out at the finish line for a bit eating Cliff bars and drinking water before heading over to a chapel in our Stake for a breakfast they were putting on. After the breakfast we walked over to Michael and Mindy's new apartment to get a tour. :) It's really nice! Much larger than their last place. :)

After that we walked back to our apartment. By this time, (11:00am) we'd walked/ran/jogged about 6.5 miles. We were pretty beat. Poor Jared was begging for piggyback rides the rest of the way home. :)

About 4 blocks from our apartment a nice little dog started following us. He came all the way home with us, and just hung out for a little bit. Turns out the Police Department has an animal control agent that they'll send out to pick up strays like this, but since it was a holiday, he wasn't in the office. We ended up giving the dog to our neighbors, who took it to their relatives in Springville to look after it until the owner could be found.

Saturday we did some of our normal chores and went for a walk before going down to Mom and Dad's for lunch. We had fun with them. We ended up leaving a little early to get Abby home for a nap. We also borrowed Edge of Tomorrow from Mom and Dad. I enjoyed it. Brittney prefers movies with more human interaction in them. :) 

Church was good today. I signed "I love you" to Abby before I left for meetings this morning, and she just laughed and got all bashful. It was adorable! (We've never signed it to her before, since she doesn't learn it in her signing show) Then I blew her a kiss, and she blew one back! She gets cuter all the time. We're happy she's in our family. :) Love you all!
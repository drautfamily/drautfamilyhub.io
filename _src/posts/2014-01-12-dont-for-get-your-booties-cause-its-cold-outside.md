    Title: Don't for get your booties, cause it's cold outside!
    Date: 2014-01-12T11:37:25
    Tags: Weekly Letters

Hey Family,

Hope you're all doing well. :) We've had a good first week back to school and work. We're slowly re-adjusting to our routine. :) School has been going well for Brittney, the kids haven't been too rowdy after break. :) I'm enjoying the semester so far too, all of my classes are pretty interesting, and the workload should be a bit lighter than last semester, so I'm looking forward to that.

Tuesday I drove up to the storehouse in Lindon to pick up some food storage that we ordered. We've realized that having food storage brings us a lot of peace, especially thinking about the recent storms and chemical spill in West VA. We're glad we got it. :)

Thursday night Brittney's parents called her just to chat, and we found out that her dad was coming out to visit this weekend! He arrived Friday night and we've had fun spending time with him.

Friday night before he arrived Brittney, Kristine, and I went out to CPK for dinner, and then came home and watched Jingle all the way. I forgot how funny some parts of that movie were. :) Brittney's dad got here around 9:30, and we all went to bed after he took Kristine home.

Saturday morning Brittney had a big church meeting that she went to, and Kristine and Robert went skiing with their dad. I stayed home and kind of did some homework. We basically get to pick our own project for my Software Engineering course this semester, but we have to get at least 6 people together to work on it with us. I proposed to make an online version of Greebles, so I spent most of Saturday doing research and preparing just in case enough people joined my team. (Sadly, it doesn't look good so far, but oh well. :) )

When they got back from skiing we all went out to Cafe Rio for lunch, and then we just chilled the rest of the afternoon. We watch the new White Collar, Brittney watch Downton Abbey, we played a bunch of Dutch Blitz, and then we all went out for FroYo. It was a good day. We love you all!
    Title: Ward Conference
    Date: 2015-10-11T10:53:05
    Tags: Weekly Letters

Hey Family! Sorry its been a little while again since we last wrote. Happy Birthday Tiffany! Happy Anniversary Mindy and Michael! And Happy Early Birthday to Dad! :) 

We've had a good week. Abby has been starting to talk more and more, and is trying to say every new word that she can. She has also been using her imagination a lot more. She likes to play with her doll and pretend to put it to sleep and wake it up. She also was pretending that her washcloth was a turtle in the bathtub the other day. :) 

<!-- more -->

Monday night Michael dropped by to say hi. We hadn't seen him in a little while so that was fun. He stayed to watch Studio C with us too. The first 20 minutes were pretty bad, but the last 3 sketches had me laughing out loud. Awwwww yeeaaaaa. :) 

The rest of the week was pretty regular. Brittney had YW stuff Tuesday and Wednesday night, I had EQ stuff Thursday night, and Friday night we just relaxed and went to bed early since I was taking the GRE Saturday morning.

The GRE was tough! I got there a little bit before the testing center opened at 7 am. I finally sat down to start the test around 7:30, and I walked out around 11:30. I was most nervous about the writing portion, so I was grateful when I saw that was first and got it out of the way early. I think I did pretty well on it. The verbal and quantitative sections were interesting. The verbal sections were tough, but about the same as the practice sections I did. The Quantitative section was a bit harder though. I never timed myself when doing the practice sections, which was a mistake. Oh well. :) I scored 160 (~80th percentile) on the verbal, and 156 (~65th percentile) on the quantitative. So decent, but not amazing. The good news is the U of U doesn't have a minimum cut off for GRE scores. :) I should wrap up my application this week, and then it's the waiting game.

After the test Saturday was a pretty regular day. We went grocery shopping, cleaned the house a bit, did the budget, etc. Kristine and Spencer hung out with us too for a little while, which was fun. That night we watched Facing the Giants. I liked it a lot. :) 

Brittney has been working this week on quilting a baby blanket for Abby's doll. We're going to build a little doll's bed for her using some extra wood we have as one of her Christmas gifts. The quilt turned out well, hopefully the bed does too. :)

Work has been going well for me too. I've been spending a lot of time helping to train a couple of new contingent employees that joined our team two weeks ago, Mark and Marlon. They're both great guys that want to do a good job, so we're glad to have them! I'm also presenting this Tuesday at SORT. (The Church's Technology Conference) I'm looking forward to it, it's not as scary as it was last year. :) I'm also looking forward to attending some of the other sessions since our team isn't under the gun this year.

Today was Ward Conference. It was great. I'm not sure if it was great because it was Ward Conference, or if it was because Abby was in nursery so I could actually pay attention... :) Either way, I'll take it. The Gospel is true! Love you all!






    Title: Happy Bastille Day!
    Date: 2013-07-14T17:36:01
    Tags: Weekly Letters

Hey Family,

We're glad you guys made it home safely, it was good to have you out here for a bit. :)

We've had a pretty good weekend. Friday was a pretty regular day at work, and then Friday evening Ben Avery came down from Bountiful for a bit. We went to dinner at Zupas and then went up the Canyon to hike and watch the stars for a bit, he loves it up there. We ended up getting home around 11, and we were exhausted. :)

Saturday morning we slept in a bit, and then ran some errands before having lunch and cleaning out the car. We spent most of the afternoon relaxing. We watched some White Collar, and then Brittney read and took a nap while I kept working on Greebles. :)

We're pretty upset about where White Collar is going right now. We just finished the second season, and started the third. So it looks like Neal and Moz are going to run away with the treasure, after all they've been through! We're about to stop watching. :P

Saturday night we went and saw <a href="http://www.thecroodsmovie.com/" target="_blank">The Croods</a> at the dollar theatre, and we're highly recommending it. :) It made us laugh, cry, and had a lot of really good messages. Definitely one of the best movies I've seen in a while. So go see it. :)

Church today was good. I finally got a home teaching assignment! Sacrament meeting was focused on baptismal covenants, it was good. Our home teachers came by this afternoon as well; we enjoyed getting to know them a bit. The Gospel is true, we love you all!
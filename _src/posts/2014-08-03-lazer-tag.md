    Title: Lazer Tag!
    Date: 2014-08-03T15:44:31
    Tags: Weekly Letters

Dear Family,

Hope you're all doing well! Any luck finding Joseph's cell phone? We haven't seen it here anywhere. Sorry.

Let's see...this week...Abby has been sleeping through the night most of the time! Yay. :) She's still as smiley and adorable as ever. It looks like she's starting to teethe though, she likes to suck and chew on her fingers all the time now. She has become a profuse drool fountain. :)

I worked from home on Wednesday because I was feeling a little sick. We're grateful that I have a job where that's possible. :) Work has been going well, it was a busy week with lots to do. That's always better than the alternative. :)

Thursday Brittney tutored Jordan in the afternoon instead of the morning, so I took the train down to Draper and then drove the rest of the way home with Brittney. Then on Friday, Brittney took Kristine to the airport in the afternoon, and so we drove home together again, which was nice. :)

Friday night Heather babysat for us while we went to the Temple, which was really nice. On Saturday we got up around 7:30 to get ready for a yard sale that another couple in our complex organized. I think there were 4 apartments that participated, and we all did pretty well  I think. We netted about $20. We were buying things from each other before anyone else even showed up. Brittney got a playmat toy thing for Abby from the Youngs, which she was excited about. She's been looking for one for a while, but they were all kind of expensive. Also from the Youngs, I got a netbook. They've had it for a few years, but just never use it. It still works, and they were selling it for $20. Done. :) I spent most of yesterday getting a new linux distribution installed on it.

After lunch and the yard sale, we went to the Bean museum to see the new exhibits. We weren't there too long, but it was fun. We enjoyed seeing the exhibit on Elder Packer's life and artwork. They also had some HUGE animals! I didn't remember seeing a full-size elephant there before!

After the museum we came home and cleaned house a bit, I grabbed some groceries, we just relaxed. Heather came over in the evening and we played lazer tag with the three of us. (I have Mike and I's old guns, and Heather and Brittney played with them earlier this week. I had no idea Brittney loved lazer tag so much, but apparently she does, so I found two more guns on ebay for $20 so that 4 of us could play together. They arrived yesterday.) It was a bunch of fun. Heather dislikes how accurate our guns are though...she didn't tag me at all. :D After that we just watched White Collar and ate ice cream. :)

Today has been busy, but good. I subbed for the Sunday School President in Ward Council this morning for the first time, that was fun. The normal meetings were good, and then I helped the young men with fast offerings this afternoon. I haven't done that in forever. :) Love you all!